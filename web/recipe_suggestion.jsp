<%-- 
    Document   : recipe
    Created on : Jul 14, 2020, 3:12:49 PM
    Author     : Kim Anh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html>
<html>
    <title>Cơm nhà</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/recipe_w3.css">
    <link rel="stylesheet" type="text/css" href="css/recipe_theme_green.css">
    <link rel="stylesheet" type="text/css" href="css/recipe_awesome.css">

    <link rel="stylesheet" type="text/css" href="css/w3.css">
    <link rel="stylesheet" type="text/css" href="css/poppins.css">
    <link rel="stylesheet" type="text/css" href="css/awesome.css">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/homepage/w3.css">
    <link rel="stylesheet" type="text/css" href="css/homepage/css?family=Raleway">
    <link rel="stylesheet" type="text/css" href="css/homepage/font-awesome.min.css">
    <body class="w3-content  w3-white" style="max-width:1500px;">
        <%@include file="homepage_header.jsp" %>
        <div class="w3-row-padding w3-center w3-margin-top">
            <div class="w3-third">
                <div class="w3-card w3-container" style="min-height:460px">
                    <h3 class="w3-xlarge">${requestScope.F1.name}</h3><br>
                    <img src="${requestScope.F1.img}" style="width: 95%"/>
                    <hr/>
                    <table style="text-align: left; width: 100%">
                        <tbody>
                            <c:forEach items="${requestScope.F1.foodIngredientCollection}" var="ingredient">
                                <tr>
                                    <td>${fn:replace(ingredient.name, '-', '')}</td>
                                    <c:if test="${ingredient.quantity != '0'}" var="checkQuantity">
                                        <td>${ingredient.quantity}</td>
                                    </c:if>
                                    <c:if test="${!checkQuantity}">
                                        <td>-</td>
                                    </c:if>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <hr/>
                    <p style="color: #4CAF50"><a href="${requestScope.F1.link}">hướng dẫn</a></p>
                    <hr/>
                </div>
            </div>
            <div class="w3-third">
                <div class="w3-card w3-container" style="min-height:460px">
                    <h3 class="w3-xlarge">${requestScope.F2.name}</h3><br>
                    <img src="${requestScope.F2.img}" style="width: 95%"/>
                    <hr/>
                    <table style="text-align: left; width: 100%">
                        <tbody>
                            <c:forEach items="${requestScope.F2.foodIngredientCollection}" var="ingredient">
                                <tr>
                                    <td>${fn:replace(ingredient.name, '-', '')}</td>
                                    <c:if test="${ingredient.quantity != '0'}" var="checkQuantity">
                                        <td>${ingredient.quantity}</td>
                                    </c:if>
                                    <c:if test="${!checkQuantity}">
                                        <td>-</td>
                                    </c:if>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <hr/>
                    <p style="color: #4CAF50"><a href="${requestScope.F2.link}">hướng dẫn</a></p>
                    <hr/>
                </div>
            </div>
            <div class="w3-third">
                <div class="w3-card w3-container" style="min-height:460px">
                    <h3 class="w3-xlarge">${requestScope.F3.name}</h3><br>
                    <img src="${requestScope.F3.img}" style="width: 95%"/>
                    <hr/>
                    <table style="text-align: left; width: 100%">
                        <tbody>
                            <c:forEach items="${requestScope.F3.foodIngredientCollection}" var="ingredient">
                                <tr>
                                    <td>${fn:replace(ingredient.name, '-', '')}</td>
                                    <c:if test="${ingredient.quantity != '0'}" var="checkQuantity">
                                        <td>${ingredient.quantity}</td>
                                    </c:if>
                                    <c:if test="${!checkQuantity}">
                                        <td>-</td>
                                    </c:if>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                    <hr/>
                    <p style="color: #4CAF50"><a href="${requestScope.F3.link}">hướng dẫn</a></p>
                    <hr/>
                </div>
            </div>
        </div>
        <hr>
        <br/>

        <c:if test="${requestScope.INSERT == null || requestScope.INSERT == false}" var="insert">
            <form action="MainController?button=btnSaveRecipe" method="POST">
                
            <input type="hidden" name="id" value="${requestScope.RECIPE.id}" />
            <input type="hidden" name="name" value="${requestScope.RECIPE.name}" />
            <input type="hidden" name="category" value="${requestScope.RECIPE.category}" />
            <input type="hidden" name="price" value="${requestScope.RECIPE.price}" />
            
            <input type="hidden" name="f1" value="${requestScope.F1.id}" />
            <input type="hidden" name="f2" value="${requestScope.F2.id}" />
            <input type="hidden" name="f3" value="${requestScope.F3.id}" />
                <div class="w3-center">
                    <button class="w3-button" style="width: 50%; background-color: lightgreen">yêu thích</button>
                </div>
            </form>

        </c:if>
        <c:if test="${!insert}">
            <div class="w3-center">
                <a class="w3-button" href="#" style="width: 50%; background-color: #e91e63">đã yêu thích</a>
            </div>
        </c:if>

        <br/>
        <div class="w3-center">
            <a class="w3-button" href="MainController?button=btnHome" style="width: 50%; background-color: lightgray">trang chủ</a>
        </div>
        <hr/>


    </body>

</html>
