<%-- 
    Document   : recipe
    Created on : Jul 14, 2020, 3:12:49 PM
    Author     : Kim Anh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html>
<html>
    <title>Cơm nhà</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/recipe_w3.css">
    <link rel="stylesheet" type="text/css" href="css/recipe_theme_green.css">
    <link rel="stylesheet" type="text/css" href="css/recipe_awesome.css">

    <link rel="stylesheet" type="text/css" href="css/w3.css">
    <link rel="stylesheet" type="text/css" href="css/poppins.css">
    <link rel="stylesheet" type="text/css" href="css/awesome.css">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/homepage/w3.css">
    <link rel="stylesheet" type="text/css" href="css/homepage/css?family=Raleway">
    <link rel="stylesheet" type="text/css" href="css/homepage/font-awesome.min.css">
    <body class="w3-content  w3-white" style="max-width:1500px;">
        <!-- Header -->
        <!--        <header class="w3-container w3-padding" id="myHeader" style="background-image: url(images/food-and-bevarge-header.jpg); background-repeat: no-repeat; background-position: center; background-size: cover; opacity: 0.7">
                    <div class="mySlides w3-animate-opacity">
                        <div class="w3-center" style="color: #ffffff">
                            <h4>Mâm cơm đơn giản cho bữa ăn ngon</h4>
                            <h1 class="w3-xxxlarge w3-animate-bottom">CƠM NHÀ</h1>
                        </div>
                    </div>
                </header>-->
        <%@include file="homepage_header.jsp" %>
        <div class="w3-row-padding w3-center w3-margin-top">
            <c:if test="${requestScope.RECIPE == null}">
                <h1>NULL</h1>
            </c:if>
            <input type="hidden" name="recipeId" value="${requestScope.RECIPE.id}" />
            <c:forEach items="${requestScope.RECIPE.foodRecipeCollection}" var="food" varStatus="counter">
                <input type="hidden" name="f${counter.count}" value="${food.foodId.id}" />
                <div class="w3-third">
                    <div class="w3-card w3-container" style="min-height:460px">
                        <h3 class="w3-xlarge">${food.foodId.name}</h3><br>
                        <img src="${food.foodId.img}" style="width: 95%"/>
                        <hr/>
                        <table style="text-align: left; width: 100%">
                            <tbody>
                                <c:forEach items="${food.foodId.foodIngredientCollection}" var="ingredient">
                                    <tr>
                                        <td>${fn:replace(ingredient.name, '-', '')}</td>
                                        <c:if test="${ingredient.quantity != '0'}" var="checkQuantity">
                                            <td>${ingredient.quantity}</td>
                                        </c:if>
                                        <c:if test="${!checkQuantity}">
                                            <td>-</td>
                                        </c:if>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                        <hr/>
                        <p style="color: #4CAF50"><a href="${food.foodId.link}">hướng dẫn</a></p>
                        <hr/>
                    </div>
                </div>
            </c:forEach>
        </div>
        <hr>
        <br/>

        <c:if test="${requestScope.WHOSE == 'their'}">
            <c:if test="${requestScope.INSERT == null || requestScope.INSERT == false}" var="insert">
                <div class="w3-center">
                    <a class="w3-button" href="MainController?button=btnLike&recipe=${requestScope.RECIPE.id}" style="width: 50%; background-color: lightgreen">yêu thích</a>
                </div>
            </c:if>
            <c:if test="${!insert}">
                <div class="w3-center">
                    <a class="w3-button" href="MainController?button=btnLike&recipe=${requestScope.RECIPE.id}" style="width: 50%; background-color: #e91e63">đã yêu thích</a>
                </div>
            </c:if>
        </c:if>
        
        
        <br/>
        <div class="w3-center">
            <a class="w3-button" href="MainController?button=btnHome" style="width: 50%; background-color: lightgray">trang chủ</a>
        </div>
        <hr/>


    </body>

</html>
