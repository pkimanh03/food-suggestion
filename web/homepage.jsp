
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <title>Cơm nhà </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/w3.css">
    <link rel="stylesheet" type="text/css" href="css/poppins.css">
    <link rel="stylesheet" type="text/css" href="css/awesome.css">



    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/homepage/w3.css">
    <link rel="stylesheet" type="text/css" href="css/homepage/css?family=Raleway">
    <link rel="stylesheet" type="text/css" href="css/homepage/font-awesome.min.css">
    <style>
        body,h1,h2,h3,h4,h5 {font-family: "Poppins", sans-serif}
        body {font-size: 16px;}
        img {margin-bottom: -8px;}
        /*.mySlides {display: none;}*/
    </style>
    <body class="w3-content w3-black" style="max-width:1500px;">
        <%@include file="homepage_header.jsp" %>
        <c:if test="${requestScope.FOODS != null && requestScope.CATEGORIES != null}">
            <div class="w3-padding-64 w3-center w3-white" onscroll="updateViewPagination()">
                <h1 class="w3-jumbo"><b>Hôm nay ăn gì nhỉ?</b></h1>
                <p class="w3-large">Hãy chọn món bạn yêu thích</p>
                <div class="w3-container">
                    <form action="MainController?button=btnHome" method="POST">
                        <div class="w3-section w3-bottombar w3-padding-16">
                            <span class="w3-margin-right">LỌC:</span> 
                            <button class="w3-button w3-black" type="submit">tất cả</button>
                            <c:forEach items="${requestScope.CATEGORIES}" var="c">
                                <button class="w3-button w3-white" id="${c.id}" type="submit" name="category" value="${c.id}">${c.name}</button>
                            </c:forEach>
                        </div>
                    </form>
                </div>

                <div class="w3-row-padding" style="margin-top:64px">
                    <c:forEach items="${requestScope.FOODS}" var="food" varStatus="counter">
                        <div class="w3-half w3-section">
                            <ul class="w3-ul w3-card w3-hover-shadow">
                                <li class="w3-red w3-xlarge w3-padding-32" value="${food.name}">${food.name}</li>
                                <li class="w3-padding-16">
                                    <img src="${food.imgLink}" />
                                </li>
                                <li class="w3-light-grey w3-padding-24">
                                    <button class="w3-button w3-black w3-padding-large" onclick="document.getElementById('${food.id}').style.display = 'block'">chi tiết</button>
                                </li>
                            </ul>
                        </div>
                        <!--Ingredient Modal-->
                        <div id="${food.id}" class="w3-modal w3-animate-opacity">
                            <div class="w3-modal-content" style="padding:32px">
                                <div class="w3-container w3-white">
                                    <i onclick="document.getElementById('${food.id}').style.display = 'none'" class="w3-xlarge w3-button w3-transparent w3-right w3-xlarge">x</i>
                                    <h2 class="w3-wide">Nguyên liệu món ăn</h2>
                                    <table style="text-align: left; width: 100%">
                                        <tbody>
                                            <c:forEach items="${food.foodIngredientCollection}" var="ingredient">
                                                <tr>
                                                    <td>${fn:replace(ingredient.name, '-', '')}</td>
                                                    <c:if test="${ingredient.quantity != '0'}" var="checkQuantity">
                                                        <td>${ingredient.quantity}</td>
                                                    </c:if>
                                                    <c:if test="${!checkQuantity}">
                                                        <td>-</td>
                                                    </c:if>
                                                </tr>
                                            </c:forEach>
                                        <hr/>
                                        <tr>
                                            <td><b>Giá tạm tính cho khẩu phần 4 người</b></td>
                                            <td>
                                                <c:if test="${food.price < 20000.0}" var="testPrice">
                                                    chưa rõ
                                                </c:if>
                                                <c:if test="${!testPrice}">
                                                    <b><fmt:formatNumber type="number" pattern="###,###"  value="${food.price * 4}" />đ</b>
                                                </c:if>
                                            </td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <hr/>
                                    <button class="w3-button w3-block w3-padding-large w3-red w3-margin-bottom" onclick="document.getElementById('download').style.display = 'none'"><a href="${food.link}">hướng dẫn</a></button>
                                    <button class="w3-button w3-block w3-padding-large w3-red w3-margin-bottom" onclick="document.getElementById('download').style.display = 'none'"><a href="MainController?button=btnPrintPDF&id=${food.id}">tải về</a></button>
                                </div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
                <br>
                <div id="paging-button">
                    <form action="MainController?button=btnHome" method="POST">
                        <input type="hidden" value="${requestScope.CATEGORY_PARAM}" name="category"/>
                        <button id="buttonPrevious" class="w3-button w3-teal w3-padding-large" name="page" value="${requestScope.CURRENT_PAGE - 1}" type="submit">trước</button>
                        <p class="w3-button w3-teal w3-padding-large">${requestScope.CURRENT_PAGE}</p>
                        <button id="buttonNext" class="w3-button w3-teal w3-padding-large" name="page" value="${requestScope.CURRENT_PAGE + 1}" type="submit">sau</button>
                    </form>
                </div>
            </div>

        </c:if>

        <footer class="w3-container w3-padding-32 w3-light-grey w3-center w3-xlarge">
            <div class="w3-section">
            </div>
            <p class="w3-medium">Powered by <a href="localhost:8084/MainController?button=" target="_blank" class="w3-hover-text-green">comnha.com</a></p>
        </footer>
    </body>
    <script>
    </script>
</html>
