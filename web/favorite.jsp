
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <title>Cơm nhà </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/w3.css">
    <link rel="stylesheet" type="text/css" href="css/poppins.css">
    <link rel="stylesheet" type="text/css" href="css/awesome.css">
    <style>
        body,h1,h2,h3,h4,h5 {font-family: "Poppins", sans-serif}
        body {font-size: 16px;}
        img {margin-bottom: -8px;}
        /*.mySlides {display: none;}*/
    </style>
    <body class="w3-content w3-black" style="max-width:1500px;">
        <%@include file="homepage_header.jsp" %>

        <c:if test="${requestScope.MY_RECIPE != null &&  not empty requestScope.MY_RECIPE}" var="my_recipe">
            <div class="w3-padding-64 w3-center w3-white">
                <h1 class="w3-jumbo"><b>Yêu thích</b></h1>
                <p class="w3-large">Những gợi ý bạn đã lưu</p>
                <div class="w3-row-padding" style="margin-top:64px">
                    <c:forEach items="${requestScope.MY_RECIPE}" var="recipe" varStatus="counter">
                        <!--<div>-->
                        <div class="w3-third w3-container w3-margin-bottom">
                            <p style="width:100%" class="w3-hover-opacity"><b>${recipe.name}</b></p>
                            <div class="w3-container w3-white">
                                <c:forEach items="${recipe.foodRecipeCollection}" var="foodRecipe">
                                    <p style="text-align: center">${foodRecipe.foodId.name}</p>
                                </c:forEach>
                                <p style="width:100%" class="w3-hover-opacity"><b><fmt:formatNumber type="number" pattern="###,###"  value="${recipe.price}" />đ</b></p>
                            </div>
                            <form action="MainController" method="POST">
                                <input type="hidden" name="recipe" value="${recipe.id}" />
                                <input type="hidden" name="whose" value="me"/>
                                <button id="buttonPrevious" style="visibility: visible" class="w3-button w3-teal w3-padding-large" name="button" value="btnRecipeDetails">chi tiết</button>    
                            </form>
                        </div>
                        <!--</div>-->
                    </c:forEach>
                </div>
                <br>
            </div>
        </c:if>
        <c:if test="${!my_recipe}">
            <div class="w3-padding-64 w3-center w3-white">
                <h1 class="w3-jumbo"><b>Yêu thích</b></h1>
                <p class="w3-large">Bạn vẫn chưa lưu công thức nào</p>
            </div>
        </c:if>

        <c:if test="${requestScope.THEIR_RECIPE != null &&  not empty requestScope.THEIR_RECIPE}" var="their_recipe">
            <div class="w3-padding-64 w3-center w3-white">
                <h1 class="w3-jumbo"><b>Người khác cũng thích</b></h1>
                <p class="w3-large">Thử xem có gì mới lạ</p>
                <div class="w3-row-padding" style="margin-top:64px">
                    <c:forEach items="${requestScope.THEIR_RECIPE}" var="recipe" varStatus="counter">
                        <!--<div>-->
                        <div class="w3-third w3-container w3-margin-bottom">
                            <p style="width:100%" class="w3-hover-opacity"><b>${recipe.name}</b></p>
                            <div class="w3-container w3-white">
                                <c:forEach items="${recipe.foodRecipeCollection}" var="foodRecipe">
                                    <p style="text-align: center">${foodRecipe.foodId.name}</p>
                                </c:forEach>
                                <p style="width:100%" class="w3-hover-opacity"><b><fmt:formatNumber type="number" pattern="###,###"  value="${recipe.price}" />đ</b></p>
                            </div>
                            <form action="MainController" method="POST">
                                <input type="hidden" name="recipe" value="${recipe.id}" />
                                <input type="hidden" name="whose" value="their"/>
                                <button id="buttonPrevious" style="visibility: visible" class="w3-button w3-teal w3-padding-large" name="button" value="btnRecipeDetails">chi tiết</button>    
                            </form>

                        </div>
                        <!--</div>-->
                    </c:forEach>
                </div>
                <br>
            </div>
        </c:if>
        <footer class="w3-container w3-padding-32 w3-light-grey w3-center w3-xlarge">
            <div class="w3-section">
            </div>
            <p class="w3-medium">Powered by <a href="localhost:8084/MainController?button=btnHome" target="_blank" class="w3-hover-text-green">comnha.com</a></p>
        </footer>
    </body>
    <script>
    </script>
</html>
