<%-- 
    Document   : login
    Created on : Jul 15, 2020, 2:11:06 AM
    Author     : Kim Anh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <style>
        body {font-family: Arial, Helvetica, sans-serif;
              background-image: url('images/login.jpg');
              background-position: center;
              background-repeat: no-repeat;
              background-size: cover;
              height: 900px
        }
        * {box-sizing: border-box;}

        /* Full-width input fields */
        input[type=text], input[type=password] {
            width: 100%;
            padding: 15px;
            margin: 5px 0 22px 0;
            display: inline-block;
            border: none;
            background: #f1f1f1;
        }

        /* Add a background color when the inputs get focus */
        input[type=text]:focus, input[type=password]:focus {
            background-color: #ddd;
            outline: none;
        }

        /* Set a style for all buttons */
        button {
            background-color: #00bcd4;
            color: white;
            padding: 14px 20px;
            margin: 8px 0px;
            border: none;
            cursor: pointer;
            width: 100%;
            opacity: 0.9;
        }

        button:hover {
            opacity:1;
        }

        /* Extra styles for the cancel button */
        .cancelbtn {
            padding: 14px 20px;
            background-color: #f44336;
        }

        /* Float cancel and signup buttons and add an equal width */
        .cancelbtn, .signupbtn {
            float: left;
            width: 50%;
        }

        /* Add padding to container elements */
        .container {
            padding: 16px;
        }

        /* The Modal (background) */
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: #474e5d;
            padding-top: 50px;
        }

        /* Modal Content/Box */
        .modal-content {
            background-color: #fefefe;
            margin: 5% auto 15% auto; /* 5% from the top, 15% from the bottom and centered */
            border: 1px solid #888;
            width: 80%; /* Could be more or less, depending on screen size */
        }

        /* Style the horizontal ruler */
        hr {
            border: 1px solid #f1f1f1;
            margin-bottom: 25px;
        }

        /* The Close Button (x) */
        .close {
            position: absolute;
            right: 35px;
            top: 15px;
            font-size: 40px;
            font-weight: bold;
            color: #f1f1f1;
        }

        .close:hover,
        .close:focus {
            color: #f44336;
            cursor: pointer;
        }

        /* Clear floats */
        .clearfix::after {
            content: "";
            clear: both;
            display: table;
        }

        /* Change styles for cancel button and signup button on extra small screens */
        @media screen and (max-width: 300px) {
            .cancelbtn, .signupbtn {
                width: 100%;
            }
        }
    </style>
    <body>

        <div style="padding-top: 400px; margin-left: 800px; height: auto; font-size: 40px">
            <h1 style="font-family: serif; font-style: italic">Cơm nhà</h1>
            <button style="margin: 8px -380px;font-size: 30px" onclick="document.getElementById('id01').style.display = 'block'" style="width:auto;">Đăng nhập</button>
        </div>


        <!--        <div id="id01" class="modal">
                    <span onclick="document.getElementById('id01').style.display = 'none'" class="close" title="Close Modal">&times;</span>
                    <form class="modal-content" action="MainController">
                        <div class="container">
                            <h1>Đăng nhập</h1>
                            <hr>
                            <label for="email"><b>Tên đăng nhập</b></label>
                            <input type="text" placeholder="Enter Username" name="username" required>
        
                            <label for="psw"><b>Mật khẩu</b></label>
                            <input type="password" placeholder="Enter Password" name="password" required>
        
                            <div class="clearfix">
                                <button type="button" onclick="document.getElementById('id01').style.display = 'none'" class="cancelbtn">đóng</button>
                                <button type="submit" name="button" value="btnLogin" class="signupbtn">đăng nhập</button>
                            </div>
                        </div>
                    </form>
                </div>-->
        <div id="id01" class="modal">
            <span onclick="document.getElementById('id01').style.display = 'none'" class="close" title="Close Modal">&times;</span>
            <div class="modal-content">
                <div class="container">
                    <h1>Đăng nhập</h1>
                    <hr>
                    <label for="email"><b>Tên đăng nhập</b></label>
                    <input type="text" id="username" placeholder="Enter Username" name="username" required>

                    <label for="psw"><b>Mật khẩu</b></label>
                    <input type="password" id="password" placeholder="Enter Password" name="password" required>

                    <div class="clearfix">
                        <button type="button" onclick="document.getElementById('id01').style.display = 'none'" class="cancelbtn">đóng</button>
                        <button type="button" name="button" value="btnLogin" class="signupbtn" onclick="loginRequest()">đăng nhập</button>
                    </div>
                </div>
            </div>
        </div>

        <script>
            // Get the modal
            var modal = document.getElementById('id01');

            // When the user clicks anywhere outside of the modal, close it
            window.onclick = function (event) {
                if (event.target == modal) {
                    modal.style.display = "none";
                }
            }

            var path = '${pageContext.request.contextPath}';

            function loginRequest() {

                var username = document.getElementById('username').value;
                var password = document.getElementById('password').value;
                var xhr = new XMLHttpRequest();
                xhr.open('POST', path + "/MainController", true);
                xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                xhr.send('button=btnLogin&username=' + username + '&password=' + password);
                document.getElementById('id01').style.display = 'none'
                xhr.onreadystatechange = function () {
                    if (xhr.status !== 200) { // analyze HTTP status of the response
                        alert('Error ${xhr.status}: ${xhr.statusText}', e); // e.g. 404: Not Found
                    } else {
                        window.location='/Food/crawler.jsp';
                    }
                };
            }
        </script>

    </body>
</html>

