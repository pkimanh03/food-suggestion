
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <title>Cơm nhà </title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/w3.css">
    <link rel="stylesheet" type="text/css" href="css/poppins.css">
    <link rel="stylesheet" type="text/css" href="css/awesome.css">
    <style>
        body,h1,h2,h3,h4,h5 {font-family: "Poppins", sans-serif}
        body {font-size: 16px;}
        img {margin-bottom: -8px;}
        /*.mySlides {display: none;}*/
    </style>
    <body class="w3-content w3-black" style="max-width:1500px;">
        <%@include file="homepage_header.jsp" %>

        <c:if test="${requestScope.RECIPE_LIST != null}">
            <div class="w3-padding-64 w3-center w3-white">
                <h1 class="w3-jumbo"><b>Gợi ý cho bạn</b></h1>
                <p class="w3-large"><em>mức giá ở dưới chỉ mang tính tham khảo</em></p>
                <p class="w3-large">Khẩu phần ăn dành cho <b>${param.numOfPeople}</b> người</p>
                <div class="w3-row-padding" style="margin-top:64px">
                    <c:forEach items="${requestScope.RECIPE_LIST}" var="recipe" varStatus="counter">
                        <form action="MainController?button=btnSuggestDetails" method="POST">
                            <div class="w3-third w3-container w3-margin-bottom">
                                <p style="width:100%" class="w3-hover-opacity"><b>${recipe.name}</b></p>
                                <div class="w3-container w3-white">
                                    <c:forEach items="${recipe.foodRecipeCollection}" var="foodRecipe" varStatus="counter">
                                        <p style="text-align: center">${foodRecipe.foodId.name}</p>
                                        <input type="hidden" name="fid${counter.count}" value="${foodRecipe.foodId.id}"/>
                                    </c:forEach>
                                    <p style="width:100%" class="w3-hover-opacity"><b><fmt:formatNumber type="number" pattern="###,###"  value="${recipe.price}" />đ</b></p>
                                </div>
                                <button id="buttonPrevious" style="visibility: visible" class="w3-button w3-teal w3-padding-large" name="page" value="">chi tiết</button>
                            </div>
                                <input type="hidden" name="id" value="${recipe.id}" />
                                <input type="hidden" name="name" value="${recipe.name}"/>
                                <input type="hidden" name="price" value="${recipe.price}"/>
                                <input type="hidden" name="category" value="${recipe.category}"/>
                                <input type="hidden" name="numOfPeople" value="${requestScope.PEOPLE}" />
                        </form>
                                
                    </c:forEach>
                </div>
                <br>
            </div>

        </c:if>
        <footer class="w3-container w3-padding-32 w3-light-grey w3-center w3-xlarge">
            <div class="w3-section">
            </div>
            <p class="w3-medium">Powered by <a href="localhost:8084/MainController?button=btnHome" target="_blank" class="w3-hover-text-green">comnha.com</a></p>
        </footer>
    </body>
    <script>
    var path = '${pageContext.request.contextPath}';
    var totalPage = '${requestScope.NUM_OF_PAGE}';
    var currentPage = '${requestScope.CURRENT_PAGE}';
    var pagingDiv = document.getElementById('paging-button');
    function getPagingData() {
        var xhr = new XMLHttpRequest();
        xhr.open('GET', path + "/MainController?page=" + levelPrice, true);
        xhr.send();
        xhr.onreadystatechange = function () {
            if (xhr.status !== 200) { // analyze HTTP status of the response
                alert('Error ${xhr.status}: ${xhr.statusText}'); // e.g. 404: Not Found
            } else { // show the result
                dataXmlString = xhr.responseText;
                renderSuggestList(dataXmlString);
            }
        };
    }

    function updateViewPagination() {
        var btnPrevious = document.getElementById('buttonPrevious');
        var btnNext = document.getElementById('buttonNext');
        if (totalPage === 0) {
            document.getElementById('paging-button').setAttribute('hidden', true);
        }
        if (currentPage === totalPage) {
            document.getElementById('buttonNext').className = '';
            document.getElementById('buttonNext').innerHTML = '';
//                document.getElementById('buttonNext').setAttribute('hidden', true);

        } else {
            document.getElementById('buttonNext').setAttribute('class', 'w3-button w3-teal w3-padding-large')
            document.getElementById('buttonNext').innerHTML = 'sau';
        }
        if (currentPage === 1) {
            document.getElementById('buttonPrevious').className = '';
            document.getElementById('buttonPrevious').innerHTML = '';
//                document.getElementById('buttonPrevious').setAttribute('hidden', true);
        } else {
            document.getElementById('buttonPrevious').setAttribute('class', 'w3-button w3-teal w3-padding-large')
            document.getElementById('buttonPrevious').innerHTML = 'trước';
        }

    }
    </script>
</html>
