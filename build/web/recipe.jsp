<%-- 
    Document   : recipe
    Created on : Jul 14, 2020, 3:12:49 PM
    Author     : Kim Anh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<!DOCTYPE html>
<html>
    <title>Cơm nhà</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="css/recipe_w3.css">
    <link rel="stylesheet" type="text/css" href="css/recipe_theme_green.css">
    <link rel="stylesheet" type="text/css" href="css/recipe_awesome.css">

    <link rel="stylesheet" type="text/css" href="css/w3.css">
    <link rel="stylesheet" type="text/css" href="css/poppins.css">
    <link rel="stylesheet" type="text/css" href="css/awesome.css">

    <body>
        <!-- Header -->
        <header class="w3-container w3-padding" id="myHeader" style="background-image: url(images/food-and-bevarge-header.jpg); background-repeat: no-repeat; background-position: center; background-size: cover; opacity: 0.7">
            <div class="mySlides w3-animate-opacity">
                <div class="w3-center">
                    <h4>Mâm cơm đơn giản cho bữa ăn ngon</h4>
                    <h1 class="w3-xxxlarge w3-animate-bottom">CƠM NHÀ</h1>
                </div>
            </div>
        </header>
        <div class="w3-row-padding w3-center w3-margin-top" id="data">

        </div>
        <hr>
        <div class="w3-center">
            <a class="w3-button" style="width: 45%; background-color: lightgreen">Hài lòng</a>
            <a class="w3-button" style="width: 45%; background-color: lightcoral">Không hài lòng</a>
        </div>
        <br/>
        <div class="w3-center">
            <a class="w3-button" style="width: 50%; background-color: lightgray">trang chủ</a>
        </div>
        <hr/>
    </body>
    <script>
        var path = '${pageContext.request.contextPath}';
        var xmlData = '${requestScope.XML}';
    </script>
    <script src="js/recipe.js" type="text/javascript"></script>

</html>
