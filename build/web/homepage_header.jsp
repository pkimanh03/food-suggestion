<%-- 
    Document   : homepage_header
    Created on : Jul 17, 2020, 9:54:50 PM
    Author     : Kim Anh
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!-- Header -->
<!-- Header with Slideshow -->
<c:if test="${requestScope.DUPLICATE != null}">
    <script>
        alert('Tên đăng nhập ${requestScope.DUPLICATE} đã tồn tại')
        document.getElementById('registration').style.display = 'block'
    </script>
</c:if>
<header class="w3-display-container w3-center">
    <div class="mySlides w3-animate-opacity">
        <img class="w3-image" src="images/background.jpg" alt="Image 1" style="min-width:500px" width="1500" height="1000">
        <div class="w3-display-left w3-padding w3-hide-small" style="width:35%">
            <div class="w3-black w3-opacity w3-hover-opacity-off w3-padding-large w3-round-large">
                <h1 class="w3-xlarge">Cơm nhà</h1>
                <hr class="w3-opacity">
                <c:if test="${sessionScope.ACCOUNT != null}" var="checkAccount">
                    <p>${sessionScope.ACCOUNT.fullname} ơi!</p>
                </c:if>
                <p>Hãy trân trọng từng bữa cơm gia đình vì đấy chính là cái nôi nuôi dưỡng tâm hồn mỗi người</p>
                <form action="MainController" method="POST">
                    <p><button name="button" value="btnHome" type="submit" class="w3-button w3-block w3-green w3-round">Trang chủ</button></p>
                </form>
                <p><button class="w3-button w3-block w3-green w3-round" onclick="document.getElementById('suggest').style.display = 'block'">Gợi ý dành cho bạn</button></p>
                <c:if test="${checkAccount}">
                    <form action="MainController" method="POST">
                        <p><button name="button" value="btnFavorite" type="submit" class="w3-button w3-block w3-green w3-round">Công thức được yêu thích</button></p>
                        <p><button name="button" value="btnLogOut" type="submit" class="w3-button w3-block w3-green w3-round">Đăng xuất</button></p>
                    </form>
                </c:if>
                <c:if test="${!checkAccount}">
                    <!--<form action="MainController" method="POST">-->
                    <p><button name="button" onclick="document.getElementById('login').style.display = 'block'" class="w3-button w3-block w3-green w3-round">Đăng nhập</button></p>
                    <!--</form>-->
                </c:if>

            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="suggest" class="w3-modal w3-animate-opacity">
        <div class="w3-modal-content" style="padding:32px">
            <div class="w3-container w3-white">
                <i onclick="document.getElementById('suggest').style.display = 'none'" class="w3-xlarge w3-button w3-transparent w3-right w3-xlarge">&times;</i>
                <h2 class="w3-wide">Gợi ý dành cho bạn</h2>
                <p>Mâm cơm đơn giản cho bữa cơm ngon</p>
                <form action="MainController" method="POST">
                    <p><input required="true" class="w3-input w3-border" type="number" min="1" placeholder="Số người dùng bữa" name="numOfPeople"></p>
                    <button type="submit" name="button" value="btnSuggest" class="w3-button w3-block w3-padding-large w3-red w3-margin-bottom" onclick="document.getElementById('suggest').style.display = 'none'">Gợi ý</button>
                </form>

            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="login" class="w3-modal w3-animate-opacity">
        <div class="w3-modal-content" style="padding:32px">
            <div class="w3-container w3-white">
                <i onclick="document.getElementById('login').style.display = 'none'" class="fa fa-remove w3-xlarge w3-button w3-transparent w3-right w3-xlarge"></i>
                <h2 class="w3-wide">Đăng nhập</h2>
                <!--<p>Mâm cơm đơn giản cho bữa cơm ngon</p>-->
                <!--<form action="MainController" method="POST">-->
                <p><input required="true" class="w3-input w3-border" type="text" placeholder="tên đăng nhập" id="username" name="username"></p>
                <p><input required="true" class="w3-input w3-border" type="password" placeholder="mật khẩu" id="password" name="password"></p>
                <button type="submit" name="button" value="btnLogin" class="w3-button w3-block w3-padding-large w3-red w3-margin-bottom" onclick="loginRequest()">đăng nhập</button>
                <!--</form>-->
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div id="registration" class="w3-modal w3-animate-opacity">
        <div class="w3-modal-content" style="padding:32px">
            <div class="w3-container w3-white">
                <i onclick="document.getElementById('registration').style.display = 'none'" class="fa fa-remove w3-xlarge w3-button w3-transparent w3-right w3-xlarge"></i>
                <h2 class="w3-wide">Đăng kí</h2>
                <!--<p>Mâm cơm đơn giản cho bữa cơm ngon</p>-->
                <form action="MainController" method="POST">
                    <p><input required="true" class="w3-input w3-border" type="text" placeholder="họ và tên" name="fullname"></p>
                    <p><input required="true" class="w3-input w3-border" type="text" placeholder="tên đăng nhập" name="username"></p>
                    <p><input required="true" class="w3-input w3-border" type="password" placeholder="mật khẩu" name="password"></p>
                    <button type="submit" name="button" value="btnRegistration" class="w3-button w3-block w3-padding-large w3-red w3-margin-bottom" onclick="document.getElementById('registration').style.display = 'none'">đăng kí</button>
                </form>
            </div>
        </div>
    </div>
</header>
<script>
    var path = '${pageContext.request.contextPath}';

    function loginRequest() {

        var username = document.getElementById('username').value;
        var password = document.getElementById('password').value;
        var xhr = new XMLHttpRequest();
        xhr.open('POST', path + "/MainController", true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send('button=btnLogin&username=' + username + '&password=' + password);
        document.getElementById('login').style.display = 'none'
        xhr.onreadystatechange = function () {
            if (xhr.status !== 200) { // analyze HTTP status of the response
                alert('Error ${xhr.status}: ${xhr.statusText}', e); // e.g. 404: Not Found
            } else {
                window.location='/Food/MainController?button=btnHome';
            }
        };
    }
</script>
