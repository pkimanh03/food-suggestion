/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var foodHeader = 
'<div class="w3-third">'
    + '<div class="w3-card w3-container" style="min-height:460px">'
        + '<h3 class="w3-xlarge">:foodName</h3><br>'
        + '<img src=":foodImg" />'
        + '<hr/>'
        + '<table style="text-align: left; width: 100%">'
            + '<tbody id="ingredientRecords" >';
           
var ingrdientContainer = 
                '<tr>'
                    + '<td>:ingredientName</td>'
                    + '<td>:quantity</td>'
                + '</tr>';
var foodFooter =   '</tbody>'
        + '</table>'
        + '<hr/>'
        + '<p style="color: #4CAF50"><a href=":foodLink">hướng dẫn</a></p>'
        + '<hr/>'
    + '</div>'
+ '</div>';           
getPagingData();
function getPagingData(username, password) {
    var xhr = new XMLHttpRequest();
    xhr.open('POST', "MainController", true);
    xhr.send('button=btnLogin&username=' + username + '&password=' + password);
    xhr.onreadystatechange = function () {
        if (xhr.status !== 200) { // analyze HTTP status of the response
            alert('Error ${xhr.status}: ${xhr.statusText}', e); // e.g. 404: Not Found
        } else {
            alert('đăng nhập thành công');// show the result
//            dataXmlString = xhr.responseText;
//            showData(dataXmlString);
        }
    };
}
function showData(dataXmlString) {
    console.log('xml-string: ' + dataXmlString)
    var domParser = new DOMParser();
    var node = domParser.parseFromString(dataXmlString, "application/xml");
//    var node = domParser.parseFromString(xmlData, "application/xml");

    var mainContainer = document.getElementById("data");
    while (mainContainer.firstChild) {
        mainContainer.removeChild(mainContainer.firstChild);
    }
    console.log('hello')
    console.log('node: ' + node)
    if (node !== null) {
        var childs = node.documentElement.childNodes;
        console.log('child' + childs)
        console.log('first' + childs.length)
        for (var i = 0; i < childs.length; i++) {
            var fn = node.getElementsByTagName("food_name")[i].firstChild.nodeValue;
            var fImg = node.getElementsByTagName("food_img")[i].firstChild.nodeValue;
            var foodH = foodHeader.replace(':foodName', fn.replace('-', ''))
                    .replace(':foodImg', 'https://' + fImg);
                    
            var ingredients = node.getElementsByTagName('ingredients')[i];
            var iContainer = '';
            console.log('ingredient:' + ingredients.length)
            for (var j = 0; j < ingredients.length; j++) {
                var ing = node.getElementsByTagName('ingredient_name')[i + j].firstChild.nodeValue;
                console.log('ingredient: ' + ing)
                var quantity = '-';
                if (node.getElementsByTagName('ingredient_quantity')[i + j].firstChild.nodeValue !== '0') {
                    quantity = node.getElementsByTagName('ingredient_quantity')[i + j].firstChild.nodeValue; 
                }
                var ingredient = ingrdientContainer.replace(':ingredientName', ing.replace('-', ''))
                        .replace(':quantity', quantity);
                iContainer += ingredient;
            }
            var foodF = foodFooter.replace(':foodLink', 'https://' + node.getElementsByTagName('food_link')[i].firstChild.nodeValue);
            var div = document.createElement('div');
//            document.getElementsByClassName('w3-row-padding w3-center w3-margin-top').innerHTML = foodH + iContainer + foodF;
            div.innerHTML = foodH + iContainer + foodF;

            mainContainer.appendChild(div);
        }
    }

}

