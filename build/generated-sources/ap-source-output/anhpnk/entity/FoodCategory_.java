package anhpnk.entity;

import anhpnk.entity.Food;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-07-13T00:12:40")
@StaticMetamodel(FoodCategory.class)
public class FoodCategory_ { 

    public static volatile SingularAttribute<FoodCategory, String> name;
    public static volatile SingularAttribute<FoodCategory, String> link;
    public static volatile SingularAttribute<FoodCategory, String> id;
    public static volatile CollectionAttribute<FoodCategory, Food> foodCollection;

}