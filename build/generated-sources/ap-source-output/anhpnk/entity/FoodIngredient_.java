package anhpnk.entity;

import anhpnk.entity.Food;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-07-13T00:12:40")
@StaticMetamodel(FoodIngredient.class)
public class FoodIngredient_ { 

    public static volatile SingularAttribute<FoodIngredient, String> quantity;
    public static volatile SingularAttribute<FoodIngredient, Food> foodId;
    public static volatile SingularAttribute<FoodIngredient, String> name;
    public static volatile SingularAttribute<FoodIngredient, String> id;

}