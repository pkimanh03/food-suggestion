package anhpnk.entity;

import anhpnk.entity.FoodCategory;
import anhpnk.entity.FoodIngredient;
import anhpnk.entity.Recipe;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-07-13T00:12:40")
@StaticMetamodel(Food.class)
public class Food_ { 

    public static volatile SingularAttribute<Food, String> img;
    public static volatile SingularAttribute<Food, Integer> quantity;
    public static volatile CollectionAttribute<Food, FoodIngredient> foodIngredientCollection;
    public static volatile SingularAttribute<Food, String> name;
    public static volatile SingularAttribute<Food, String> link;
    public static volatile SingularAttribute<Food, String> id;
    public static volatile CollectionAttribute<Food, Recipe> recipeCollection;
    public static volatile SingularAttribute<Food, FoodCategory> categoryId;

}