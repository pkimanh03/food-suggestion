package anhpnk.entity;

import anhpnk.entity.Food;
import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-07-13T00:12:40")
@StaticMetamodel(Recipe.class)
public class Recipe_ { 

    public static volatile SingularAttribute<Recipe, String> name;
    public static volatile SingularAttribute<Recipe, String> id;
    public static volatile SingularAttribute<Recipe, String> category;
    public static volatile SingularAttribute<Recipe, Integer> point;
    public static volatile CollectionAttribute<Recipe, Food> foodCollection;

}