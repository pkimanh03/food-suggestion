/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.utils;

import anhpnk.constant.AppConstant;
import anhpnk.jaxb.foody.IngredientList;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDate;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.util.JAXBResult;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;

import org.apache.fop.apps.FOPException;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;

/**
 *
 * @author Kim Anh
 */
public class PDFUtils {

    /**
     * Method that will convert the given XML to PDF
     *
     * @throws IOException
     * @throws FOPException
     * @throws TransformerException
     */
    public static void convertToPDF() throws IOException, FOPException, TransformerException {
        File xsltFile = new File(AppConstant.XSL_FILE_PATH);
        StreamSource xmlSource = new StreamSource(new File(AppConstant.FOOD_PDF_XML_FILE_PATH));
        FopFactory fopFactory = FopFactory.newInstance();
        FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
        OutputStream out = new FileOutputStream(AppConstant.PROJECT_PATH + "food.pdf");

        try {
            Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);
            TransformerFactory factory = TransformerFactory.newInstance();
            Transformer transformer = factory.newTransformer(new StreamSource(xsltFile));
            Result res = new SAXResult(fop.getDefaultHandler());
            transformer.transform(xmlSource, res);
        } finally {
            out.close();
        }
    }

//    public static void generatePDF(IngredientList il, String name) {
//        OutputStream out = null;
//        try {
//            File xsltFile = new File(AppConstant.XSL_FILE_PATH);
//            File pdfDir = new File("E:\\pdf");
//            pdfDir.mkdir();
//            String fileName = name + ".pdf";
//            File pdfFile = new File(pdfDir, fileName);
//
//            final FopFactory fopFactory = FopFactory.newInstance();
//            fopFactory.setUserConfig("E:\\Food\\web\\WEB-INF\\config.xml");
//            FOUserAgent fOUserAgent = fopFactory.newFOUserAgent();
//            out = new FileOutputStream(pdfFile);
//            out = new BufferedOutputStream(out);
//            Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, fOUserAgent, out);
//            TransformerFactory factory = TransformerFactory.newInstance();
//            Transformer transformer = factory.newTransformer(new StreamSource(xsltFile));
//            Source source = new StreamSource(new File(AppConstant.FOOD_PDF_XML_FILE_PATH));
//            Result result = new JAXBResult(JAXBContext.newInstance(il.getClass()));
////            result.
//            transformer.transform(source, result);
//        } catch (Exception e) {
//            e.printStackTrace();
//        } finally {
//            try {
//                out.close();
//            } catch (Exception e) {
//            }
//        }
//    }
}
