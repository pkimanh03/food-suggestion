/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.utils;

import anhpnk.crawler.BaseCrawler;
import anhpnk.crawler.FoodCategoriesCrawler;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.transform.sax.SAXSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.xml.sax.InputSource;

/**
 *
 * @author Kim Anh
 */
public class AppUtils {

    public static UUID generateUUID() {
        return UUID.randomUUID();
    }

    public static void saveToXML(String xmlFilePath, Object object) {
        try {
            JAXBContext context = JAXBContext.newInstance(object.getClass());
            Marshaller marshal = context.createMarshaller();
            marshal.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            marshal.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            marshal.marshal(object, new File(xmlFilePath));
        } catch (JAXBException ex) {
            Logger.getLogger(FoodCategoriesCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static boolean validateBeforeSaveToDB(String schemaFilePath, String xmlFilePath, Object objectList) {
        try {
            JAXBContext context = JAXBContext.newInstance(objectList.getClass());
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = schemaFactory.newSchema(new File(schemaFilePath));
            Validator validator = schema.newValidator();
            InputSource inputFile = new InputSource(new BufferedReader(new FileReader(xmlFilePath)));
            validator.validate(new SAXSource(inputFile));
            return true;
        } catch (Exception e) {
            Logger.getLogger(BaseCrawler.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }
    }

    public static String replaceInvalidCharacter(String document) {
        document = document.replaceAll("nbsp", "");
        document = document.replaceAll("hellip", "");
        document = document.replaceAll("href=\"javascript:;\" ", "");
        document = document.replaceAll(";", "");
        document = document.replaceAll("&", "");
        document = document.replaceAll("#", "");
        document = document.replaceAll("®", "");
        return document.trim();
    }

    public static String getDifferentWordInSecondString(String s1, String s2) {
        String result = "";
        String[] strList1 = s1.split(" ");
        String[] strList2 = s2.split(" ");

        List<String> list1 = Arrays.asList(strList1);
        List<String> list2 = Arrays.asList(strList2);

        List<String> intersection = new ArrayList<>(list2);
        intersection.retainAll(list1);
        List<String> tmp = new ArrayList<>(list2);
        tmp.removeAll(intersection);
        for (String s : tmp) {
            result += " " + s;
        }
        return result.trim();
    }
}
