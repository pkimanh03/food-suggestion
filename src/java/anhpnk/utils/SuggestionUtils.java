/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.utils;

import anhpnk.constant.DictionaryConstant;
import anhpnk.dtos.IngredientDTO;
import anhpnk.entity.Food;
import anhpnk.entity.FoodIngredient;
import anhpnk.entity.Ingredient;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Kim Anh
 */
public class SuggestionUtils {

    /**
     * loại bỏ những kí tự không hợp lệ trong chuỗi
     * @param name
     * @return 
     */
    private static String removeInvalidCharacter(String name) {
        name = name.replaceAll("[(].{1,}[)]{0,1}", "");
        name = name.replaceAll(" con ", "");
        name = name.replaceAll("kg/cây", "");
        name = name.replaceAll("size", "");
        name = name.replaceAll("trái/kg", "");
        name = name.replaceAll("gr", "");
        name = name.replaceAll("KG", "");
        name = name.replaceAll("quả/hộp", "");
        name = name.replaceAll("\\d", "");

        for (int i = 0; i < name.length(); i++) {
            char n = name.charAt(i);
            if (Character.isUpperCase(n) && i > 0) {
                name = name.substring(0, i);
                break;
            }
        }
        name = name.toLowerCase();
        return name.trim();
    }

    /**
     * chuyển đổi entity thành dto: thay đổi đơn vị tính và loại bỏ những entity có tên không hợp lệ
     * @param dbIngredient
     * @return 
     */
    private static List<IngredientDTO> convertToIngredientDTO(List<Ingredient> dbIngredient) {
        List<IngredientDTO> result = new ArrayList();
        IngredientDTO dto = null;
        String name = "", unit = "";
        double price = 0;
        for (Ingredient ingredient : dbIngredient) {
            name = removeInvalidCharacter(ingredient.getName().trim()).trim();
            name = name.replaceAll("heo", "lợn");
            if (name.trim().length() >= 2) {
                unit = "kg";
                price = ingredient.getPrice();
                if (ingredient.getUnit().equals("vĩ") && ingredient.getName().toLowerCase().contains("trứng")) {
                    price = ingredient.getPrice() / 30;
                } else if (ingredient.getName().toLowerCase().contains("vịt")) {
                    price = ingredient.getPrice() / 2;
                }
                if (!ingredient.getUnit().equals("kg")) {
                    unit = "cái";
                }

                dto = new IngredientDTO(name, price, unit);
                dto.setId(ingredient.getId());
                result.add(dto);
            }
        }
        return result;
    }

    /**
     * lấy ra giá của nguyên liệu cụ thể
     * @param ingredientName
     * @param dtoList
     * @return 
     */
    private static double priceForOneIngredientInFood(String ingredientName, List<IngredientDTO> dtoList) {
        if (ingredientName.contains(":") || ingredientName.contains(",")) {
            return 0;
        }
        double price = 0;
        double matchedLevel = 0;
        double min = Integer.MAX_VALUE;

        for (IngredientDTO dto : dtoList) {
            String dbName = dto.getName().trim();
            String differentWords = AppUtils.getDifferentWordInSecondString(dbName, ingredientName);
            if (differentWords.length() < ingredientName.trim().length()) {
                matchedLevel = differentWords.length();
                if (matchedLevel < min) {
                    min = matchedLevel;
                    price = dto.getPrice();
                }

            }
        }
        return price;
    }

    /**
     * chuyển đổi số lượng nguyên liệu của món ăn với đơn vị là kg
     * nếu là trứng thì giữ nguyên số lượng
     * nếu là số nguyên thì lấy số chia cho 1000
     * nếu là phân số thì lấy 0.5 nhân cho 100
     * @param quantityStr
     * @param nameStr
     * @return
     */
    private static double quantityForOneIngredientInFood(String quantityStr, String nameStr) {
        double result = 0.0;
        if (quantityStr.equals("0")) {
            return 0;
        }
        if (quantityStr.contains(" ")) {
            quantityStr = quantityStr.replaceAll(quantityStr.substring(quantityStr.lastIndexOf(" ")), "").trim();
        }
        if (!quantityStr.contains("/")) {
            if (nameStr.toLowerCase().contains("trứng")) {
                result = Integer.parseInt(quantityStr.replaceAll("\\D", "").trim());
            } else {
                String s = quantityStr.replaceAll("\\D", "").trim();
                double q = Double.parseDouble(s);
                if (s.length() > 1) {
                    s = "0." + s.charAt(0) + s.charAt(1);
                    result = Double.parseDouble(s);
                } else {
                    s = "0." + s.charAt(0);
                    result = Double.parseDouble(s);
                }
            }
        } else {
            result = 0.5 * DictionaryConstant.BASED_QUANTITY;
        }

        return result;
    }

    /**
     * tính toán giá gợi ý cho 1 món ăn bất kì với số khẩu phần ăn là 1
     * @param food món ăn
     * @param ingredients danh sách nguyên liệu mà data có
     * @return
     */
    public static double getSuggestionPrice(Food food, List<Ingredient> ingredients) {
        double price = 0;
        List<IngredientDTO> dtoList = convertToIngredientDTO(ingredients);
        for (FoodIngredient foodIngredient : food.getFoodIngredientCollection()) {
            double pr = priceForOneIngredientInFood(foodIngredient.getName(), dtoList);
            double quan = quantityForOneIngredientInFood(foodIngredient.getQuantity(), foodIngredient.getName()) / food.getQuantity();
            price += pr * quan;
        }
//        if (food.getQuantity() > 0) {
//            price = price;
//        }
        return price;
    }

    /**
     * kiểm tra xem món ăn đó có phù hợp để ăn với cơm hay không
     * @param name
     * @param category
     * @return 
     */
    public static boolean isValidFoodName(String name, String category) {
        name = name.toLowerCase().trim();
        category = category.toLowerCase();
        switch (category) {
            case DictionaryConstant.MON_CANH:
                for (String s : DictionaryConstant.CANH_EXCEPTION) {
                    if (name.contains(s)) {
                        return false;
                    }
                }
                return true;
            case DictionaryConstant.MON_CHIEN:
                for (String s : DictionaryConstant.CHIEN_EXCEPTION) {
                    if (name.contains(s)) {
                        return false;
                    }
                }
                return true;
            case DictionaryConstant.MON_KHO:
                for (String s : DictionaryConstant.KHO_EXCEPTION) {
                    if (name.contains(s)) {
                        return false;
                    }
                }
                return true;
            case DictionaryConstant.MON_OM:
                if (!name.contains(DictionaryConstant.MON_OM)) {
                    return false;
                }
                return true;
            case DictionaryConstant.MON_XAO:
                for (String s : DictionaryConstant.XAO_EXCEPTION) {
                    if (name.contains(s)) {
                        return false;
                    }
                }
                return true;
            default:
                return false;
        }
    }
    public static Food calculateQuantity(Food f, int numOfPeopple) {
        int quantityFood = f.getQuantity();
        for (FoodIngredient fi : f.getFoodIngredientCollection()) {
            String tmpQuantityStr = fi.getQuantity().replaceAll("\\D", "");
            int quantityOfI = Integer.parseInt(tmpQuantityStr);

            float quantityAfterCalculOfI = (quantityOfI / quantityFood) * numOfPeopple;
            fi.setQuantity(fi.getQuantity().replaceAll("\\d{1,3}", quantityAfterCalculOfI + ""));
        }
        System.out.println(f);
        return f;
    }
}
