/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 *
 * @author Kim Anh
 */
public class StringUtils {
    
    public static List<String> getDifferentWordInSecondString(String s1, String s2) {
        String[] strList1 = s1.split(" ");
        String[] strList2 = s2.split(" ");

        List<String> list1 = Arrays.asList(strList1);
        List<String> list2 = Arrays.asList(strList2);

        List<String> intersection = new ArrayList<>(list2);
        intersection.retainAll(list1);
        List<String> tmp = new ArrayList<>(list2);
        tmp.removeAll(intersection);
        for (String s : tmp) {
            System.out.println(s);
        }
        return tmp;
    }
}
