/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.daos;

import anhpnk.entity.Ingredient;
import anhpnk.utils.DBUtils;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author Kim Anh
 */
public class IngredientDAO implements Serializable {
    private static IngredientDAO instance;
    private static final Object LOCK = new Object();
    
    public static IngredientDAO getInstance() {
        synchronized (LOCK) {
            if (instance == null) {
                instance = new IngredientDAO();
            }
        }
        return instance;
    }
    
    public synchronized boolean insert(Ingredient ingredient) {
        EntityManager manager = DBUtils.getEntityManager();
        try {
            EntityTransaction transaction = manager.getTransaction();
            transaction.begin();
            Ingredient found = manager.find(Ingredient.class, ingredient.getId());
            if (found == null) {
                manager.persist(ingredient);
            } else {
                manager.remove(found);
                manager.persist(ingredient);
            }
            transaction.commit();
            return true;
        } catch (Exception e) {
            Logger.getLogger(IngredientDAO.class.getName()).log(Level.SEVERE, null, e);
            return false;
        } finally {
            if (manager != null) {
                manager.close();
            }
        }
    }
    
    public synchronized boolean insertAll(List<Ingredient> ingredients) {
        EntityManager manager = DBUtils.getEntityManager();
        try {
            EntityTransaction transaction = manager.getTransaction();
            transaction.begin();
            for (Ingredient ingredient : ingredients) {
                Ingredient found = manager.find(Ingredient.class, ingredient.getId());
                if (found == null) {
                    manager.persist(ingredient);
                } else {
                    manager.remove(found);
                    manager.persist(ingredient);
                }
            }
            transaction.commit();
            return true;
        } catch (Exception e) {
            Logger.getLogger(IngredientDAO.class.getName()).log(Level.SEVERE, null, e);
            return false;
        } finally {
            if (manager != null) {
                manager.close();
            }
        }
    }
    
    public List<Ingredient> getAll() throws Exception {
        List<Ingredient> result = null;
        EntityManager manager = DBUtils.getEntityManager();
        try {
            EntityTransaction transaction = manager.getTransaction();
            transaction.begin();
            Query query = manager.createNamedQuery("Ingredient.findAll", Ingredient.class);
            result = query.getResultList();
            transaction.commit();
            System.out.println("SEARCH: " + result.size());
        } catch (Exception e) {
            Logger.getLogger(IngredientDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (manager != null) {
                manager.close();
            }
        }
        return result;
    }
}
