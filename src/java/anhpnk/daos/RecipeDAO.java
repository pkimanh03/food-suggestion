/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.daos;

import anhpnk.entity.FavoriteRecipe;
import anhpnk.entity.Food;
import anhpnk.entity.FoodRecipe;
import anhpnk.entity.Recipe;
import anhpnk.utils.AppUtils;
import anhpnk.utils.DBUtils;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import javax.persistence.Query;

/**
 *
 * @author Kim Anh
 */
public class RecipeDAO implements Serializable {

    private final int PAGE_SIZE = 6;
    private static RecipeDAO instance;
    private static final Object LOCK = new Object();

    public static RecipeDAO getInstance() {
        synchronized (LOCK) {
            if (instance == null) {
                instance = new RecipeDAO();
            }
        }
        return instance;
    }

    public boolean insert(Recipe recipe) {
        EntityManager manager = DBUtils.getEntityManager();
        try {
            EntityTransaction transaction = manager.getTransaction();
            transaction.begin();
            manager.persist(recipe);
            transaction.commit();
            return true;
        } catch (Exception e) {
            Logger.getLogger(RecipeDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return false;
    }
    
    public boolean insertFullRecipe(Recipe recipe) {
        EntityManager manager = DBUtils.getEntityManager();
        try {
            EntityTransaction transaction = manager.getTransaction();
            transaction.begin();
            manager.persist(recipe);
            for (FoodRecipe food : recipe.getFoodRecipeCollection()) {
                manager.persist(food);
            }
            transaction.commit();
            return true;
        } catch (Exception e) {
            Logger.getLogger(RecipeDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return false;
    }

    public List<Recipe> getRecipeWithSortingAndPaging(int page) {
        EntityManager manager = DBUtils.getEntityManager();
        List<Recipe> result = new ArrayList();
        try {
            String sql = "SELECT r FROM Recipe r ORDER BY r.point DESC";
            Query query = manager.createQuery(sql);
            query.setFirstResult((page - 1) * PAGE_SIZE);
            query.setMaxResults(PAGE_SIZE);
            result = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            Logger.getLogger(RecipeDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return result;

    }

    public Recipe getOne(String id) {
        EntityManager manager = DBUtils.getEntityManager();
        Recipe result = null;
        try {
            Query query = manager.createNamedQuery("Recipe.findById");
            query.setParameter("id", id);
            result = (Recipe) query.getSingleResult();
            return result;
        } catch (Exception e) {
            if (e.getMessage().contains("getSingleResult()")) {
                return null;
            }
        }
        return result;
    }
    
    public boolean updatePoint(int point, Recipe r) {
        EntityManager manager = DBUtils.getEntityManager();
        try {
            EntityTransaction transaction = manager.getTransaction();
            transaction.begin();
            String sql = "UPDATE Recipe SET point = ?1 WHERE id = ?2";
            Query query = manager.createNativeQuery(sql);
            query.setParameter(1, point);
            query.setParameter(2, r.getId());
            int rs = query.executeUpdate();
            transaction.commit();
            if (rs > 0) return true;
        } catch (Exception e) {
            e.printStackTrace();
            Logger.getLogger(RecipeDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return false;
    }
    
    public int countPage() {
        EntityManager manager = DBUtils.getEntityManager();
        int page = 0;
        try {
            EntityTransaction transaction = manager.getTransaction();
            transaction.begin();
            String sql = "SELECT COUNT(r.id) FROM Recipe r";
            Query query = manager.createNativeQuery(sql);
            page = (int) query.getSingleResult();
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            Logger.getLogger(RecipeDAO.class.getName()).log(Level.SEVERE, null, e);
            return 0;
        }
        return page;
    }
    
    public List<Recipe> getPersonalFavoriteRecipe(String accountId) {
        EntityManager manager = DBUtils.getEntityManager();
        List<Recipe> result = new ArrayList<>();
        try {
            String sql = "SELECT fr FROM FavoriteRecipe fr JOIN fr.accountId a WHERE a.id = ?1";
            Query query = manager.createQuery(sql, FavoriteRecipe.class);
            query.setParameter(1, accountId);
            List<FavoriteRecipe> tmpList = query.getResultList();
            if (tmpList != null && !tmpList.isEmpty()) {
                for (FavoriteRecipe fr : tmpList) {
                    result.add(fr.getRecipeId());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            Logger.getLogger(RecipeDAO.class.getName()).log(Level.SEVERE, null, e);
        }
        return result;
    }
    
    public boolean insertFavoriteRecipe(FavoriteRecipe fr) {
        EntityManager manager = DBUtils.getEntityManager();
        try {
            EntityTransaction transaction = manager.getTransaction();
            transaction.begin();
            manager.persist(fr);
            transaction.commit();
            return true;
        } catch (Exception e) {
            Logger.getLogger(RecipeDAO.class.getName()).log(Level.SEVERE, null, e);
            return false;
        }
    }
}
