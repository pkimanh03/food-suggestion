/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.daos;

import anhpnk.entity.Food;
import anhpnk.entity.FoodCategory;
import anhpnk.entity.FoodIngredient;
import anhpnk.jaxb.foody.IngredientFood;
import anhpnk.utils.DBUtils;
import java.io.Serializable;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

/**
 *
 * @author Kim Anh
 */
public class FoodDAO implements Serializable {

    private final int PAGE_SIZE = 10;
    private static FoodDAO instance;
    private static final Object LOCK = new Object();

    public static FoodDAO getInstance() {
        synchronized (LOCK) {
            if (instance == null) {
                instance = new FoodDAO();
            }
        }
        return instance;
    }

    public synchronized boolean insert(Food food) {
        EntityManager manager = DBUtils.getEntityManager();
        try {
            EntityTransaction transaction = manager.getTransaction();
            transaction.begin();
            Food found = manager.find(Food.class, food.getId());
            if (found == null) {
                manager.persist(food);
            } else {
                manager.remove(found);
                manager.persist(food);
            }
            transaction.commit();
            return true;
        } catch (Exception e) {
            Logger.getLogger(IngredientDAO.class.getName()).log(Level.SEVERE, null, e);
            return false;
        } finally {
            if (manager != null) {
                manager.close();
            }
        }
    }

    public synchronized boolean insertCompleteFood(anhpnk.jaxb.foody.Food food) {
        EntityManager manager = DBUtils.getEntityManager();
        try {
            EntityTransaction transaction = manager.getTransaction();
            transaction.begin();
            Food fFound = manager.find(Food.class, food.getId());
            FoodCategory cFound = manager.find(FoodCategory.class, food.getCategory().getId());

            FoodCategory c = new FoodCategory(food.getCategory());
            if (cFound == null) {
                manager.persist(c);
            } else {
                c = cFound;
            }
            Food f = new Food(food);
            f.setCategoryId(c);
            if (fFound == null) {
                manager.persist(f);
            } else {
                f = fFound;
            }

            FoodIngredient iFound = null;
            for (IngredientFood foodIngzredient : food.getIngredientList().getIngredient()) {
                iFound = manager.find(FoodIngredient.class, foodIngzredient.getId());
                if (iFound == null) {
                    FoodIngredient i = new FoodIngredient(foodIngzredient);
                    i.setFoodId(f);
                    manager.persist(i);
                }
            }
            transaction.commit();
            return true;
        } catch (Exception e) {
            Logger.getLogger(FoodDAO.class.getName()).log(Level.SEVERE, null, e);
            return false;
        } finally {
            if (manager != null) {
                manager.close();
            }
        }
    }

    public List<Food> getFoodByCategory(String categoryName) {
        EntityManager manager = DBUtils.getEntityManager();
        List<Food> result = null;
        try {
            EntityTransaction transaction = manager.getTransaction();
            transaction.begin();
            String sql = "SELECT DISTINCT f FROM Food f JOIN f.categoryId c WHERE c.name LIKE ?1";
            TypedQuery<Food> categoryQuery = manager.createQuery(sql, Food.class);
            categoryQuery.setParameter(1, "%" + categoryName + "%");
            result = categoryQuery.getResultList();
        } catch (Exception e) {
            Logger.getLogger(FoodDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (manager != null) {
                manager.close();
            }
        }
        return result;
    }

    public Food getById(String id) {
        EntityManager manager = DBUtils.getEntityManager();
        Food result = null;
        try {
            Query query = manager.createNamedQuery("Food.findById");
            query.setParameter("id", id);
            result = (Food) query.getSingleResult();
        } catch (Exception e) {
            Logger.getLogger(FoodDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (manager != null) {
                manager.close();
            }
        }
        return result;
    }

    public List<Food> getFoodWithPaging(int pageNumber) {
        EntityManager manager = DBUtils.getEntityManager();
        List<Food> result = new ArrayList();
        try {
            Query query = manager.createNamedQuery("Food.findAll");
            query.setFirstResult((pageNumber - 1) * PAGE_SIZE);
            query.setMaxResults(PAGE_SIZE);
            result = query.getResultList();
        } catch (Exception e) {
            Logger.getLogger(FoodDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (manager != null) {
                manager.close();
            }
        }
        return result;
    }
    
    public List<Food> getFoodWithPagingForEachCategory(long pageNumber, String categoryId) {
        EntityManager manager = DBUtils.getEntityManager();
        List<Food> result = new ArrayList();
        try {
            String sql = "SELECT f FROM Food f JOIN f.categoryId c WHERE c.id = ?1";
            Query query = manager.createQuery(sql);
            query.setParameter(1, categoryId);
            query.setFirstResult((int) ((pageNumber - 1) * PAGE_SIZE));
            query.setMaxResults(PAGE_SIZE);
            result = query.getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            Logger.getLogger(FoodDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (manager != null) {
                manager.close();
            }
        }
        return result;
    }

    public byte[] getFoodForDetailsXML(String id1, String id2, String id3) {
        EntityManager manager = DBUtils.getEntityManager();
        String result = "";
        try {
            Query query = manager.createNativeQuery("SELECT CAST("
                    + "(SELECT "
                    + "f.id AS 'food_id', "
                    + "f.name AS 'food_name', "
                    + "(SELECT REPLACE(f.img, 'https://', ''))  AS 'food_img', "
                    + "(SELECT REPLACE(f.link, 'https://', ''))  AS 'food_link', "
                    + "f.category_id AS 'category_id', "
                    + "f.quantity AS 'food_quantity', "
                    + "(SELECT "
                    + "i.id AS 'ingredient_id', "
                    + "i.name AS 'ingredient_name', "
                    + "i.quantity AS 'ingredient_quantity' "
                    + "FROM FOOD_INGREDIENT i "
                    + "WHERE f.id = i.food_id "
                    + "FOR XML PATH('ingredient'), ROOT('ingredients'),TYPE) "
                    + "FROM FOOD f "
                    + "WHERE f.id = ?1 OR f.id = ?2 OR f.id = ?3 "
                    + "FOR XML PATH('food'), ROOT('foods'),TYPE) "
                    + "AS NVARCHAR(MAX))");
            query.setParameter(1, id1);
            query.setParameter(2, id2);
            query.setParameter(3, id3);
            
            result = (String) query.getSingleResult();
        } catch (Exception e) {
            Logger.getLogger(FoodDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (manager != null) {
                manager.close();
            }
        }
        return result.getBytes(StandardCharsets.UTF_8);
    }
    
    public int getNumberOfPage() {
        int pageNum = 0;
        EntityManager manager = DBUtils.getEntityManager();
        try {
            String countSql = "SELECT COUNT(f.id) FROM Food f";
            Query countQuery = manager.createNativeQuery(countSql);
            int countResult = (int) countQuery.getSingleResult();
            pageNum = (int) (countResult / PAGE_SIZE);
        } catch (Exception e) {
            Logger.getLogger(FoodDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (manager != null) {
                manager.close();
            }
        }
        return pageNum;
    }
    
    public long getNumberOfPageForEachCategory(String categoryId) {
        int pageNum = 0;
        EntityManager manager = DBUtils.getEntityManager();
        try {
            String countSql = "SELECT COUNT(f.id) FROM Food f JOIN f.categoryId c WHERE c.id = ?1";
            Query countQuery = manager.createQuery(countSql);
            countQuery.setParameter(1, categoryId);
            long countResult = (long) countQuery.getSingleResult();
            pageNum = (int) (countResult / PAGE_SIZE);
        } catch (Exception e) {
            e.printStackTrace();
            Logger.getLogger(FoodDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (manager != null) {
                manager.close();
            }
        }
        return pageNum;
    }
}
