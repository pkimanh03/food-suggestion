/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.daos;

import anhpnk.entity.Account;
import anhpnk.utils.AppUtils;
import anhpnk.utils.DBUtils;
import java.io.Serializable;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

/**
 *
 * @author Kim Anh
 */
public class AccountDAO implements Serializable {

    private static final Object LOCK = new Object();
    private static AccountDAO instance;

    public static AccountDAO getInstance() {
        synchronized (LOCK) {
            if (instance == null) {
                instance = new AccountDAO();
            }
        }
        return instance;
    }

    public String checkLogin(String username, String password) {
        EntityManager manager = DBUtils.getEntityManager();
        String role = "INVALID";
        try {
            Query query = manager.createNamedQuery("Account.checkLogin");
            query.setParameter("username", username);
            query.setParameter("password", password);
            Account account = (Account) query.getSingleResult();
            role = account.getRole();
        } catch (Exception e) {
            if (e.getMessage().contains("getSingleResult()")) {
                return role;
            }
        }
        return role;
    }
    
    public boolean createUser(Account account) {
        EntityManager manager = DBUtils.getEntityManager();
        try {
            EntityTransaction transaction = manager.getTransaction();
            transaction.begin();
            manager.persist(account);
            transaction.commit();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        System.out.println("Created");
        return true;
    }
    
    public boolean isExisted(String username) {
        EntityManager manager = DBUtils.getEntityManager();
        try {
            Query query = manager.createNamedQuery("Account.findByUsername");
            query.setParameter("username", username);
            Account account = (Account) query.getSingleResult();
            if (account != null) return true;
            else return false;
        } catch (Exception e) {
            if (e.getMessage().contains("getSingleResult()")) {
                return true;
            }
        }
        return false;
    }
}
