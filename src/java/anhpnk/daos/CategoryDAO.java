/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.daos;

import anhpnk.entity.FoodCategory;
import anhpnk.entity.Ingredient;
import anhpnk.utils.DBUtils;
import java.io.Serializable;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

/**
 *
 * @author Kim Anh
 */
public class CategoryDAO implements Serializable {
    private static CategoryDAO instance;
    private static final Object LOCK = new Object();
    
    public static CategoryDAO getInstance() {
        synchronized (LOCK) {
            if (instance == null) {
                instance = new CategoryDAO();
            }
        }
        return instance;
    }
    
    public synchronized boolean insert(FoodCategory category) {
        EntityManager manager = DBUtils.getEntityManager();
        try {
            EntityTransaction transaction = manager.getTransaction();
            transaction.begin();
            Ingredient found = manager.find(Ingredient.class, category.getId());
            if (found == null) {
                manager.persist(category);
            } else {
                manager.remove(found);
                manager.persist(category);
            }
            transaction.commit();
            return true;
        } catch (Exception e) {
            Logger.getLogger(IngredientDAO.class.getName()).log(Level.SEVERE, null, e);
            return false;
        } finally {
            if (manager != null) {
                manager.close();
            }
        }
    }
    
    public List<FoodCategory> getAll() {
        EntityManager manager = DBUtils.getEntityManager();
        List<FoodCategory> result = null;
        try {
            Query query = manager.createNamedQuery("FoodCategory.findAll");
            result = query.getResultList();
        } catch (Exception e) {
            Logger.getLogger(CategoryDAO.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            if (manager != null) {
                manager.close();
            }
        }
        return result;
    }
}
