/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.daos;

import anhpnk.entity.FoodIngredient;
import anhpnk.entity.Ingredient;
import anhpnk.utils.DBUtils;
import java.io.Serializable;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;

/**
 *
 * @author Kim Anh
 */
public class FoodIngredientDAO implements Serializable {
    private static FoodIngredientDAO instance;
    private static final Object LOCK = new Object();
    
    public static FoodIngredientDAO getInstance() {
        synchronized (LOCK) {
            if (instance == null) {
                instance = new FoodIngredientDAO();
            }
        }
        return instance;
    }
    
    public synchronized boolean insert(FoodIngredient ingredient) {
        EntityManager manager = DBUtils.getEntityManager();
        try {
            EntityTransaction transaction = manager.getTransaction();
            transaction.begin();
            Ingredient found = manager.find(Ingredient.class, ingredient.getId());
            if (found == null) {
                manager.persist(ingredient);
            } else {
                manager.remove(found);
                manager.persist(ingredient);
            }
            transaction.commit();
            return true;
        } catch (Exception e) {
            Logger.getLogger(IngredientDAO.class.getName()).log(Level.SEVERE, null, e);
            return false;
        } finally {
            if (manager != null) {
                manager.close();
            }
        }
    }
}
