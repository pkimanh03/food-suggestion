/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.crawler;

import anhpnk.constant.AppConstant;
import anhpnk.daos.FoodDAO;
import anhpnk.jaxb.foody.Category;
import anhpnk.jaxb.foody.Food;
import anhpnk.utils.AppUtils;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 *
 * @author Kim Anh
 */
public class FoodPageCrawler extends BaseCrawler {

    public static List<Food> listAllFood = new ArrayList();
    private List<String> linkList;
    private String url;
    private Category category;

    public FoodPageCrawler(String url, Category category, ServletContext context) {
        super(context);
        this.url = url;
        this.category = category;
        linkList = new ArrayList<>();
    }

    private void staxParserForEachPage(String document) {
        XMLEvent event = null;
        try {
            document = AppUtils.replaceInvalidCharacter(document);
            XMLEventReader eventReader = parseStringToXMLEventReader(document);

            String tagName = "";
            StartElement startElement = null;
            Attribute attribute = null;
            String link = "";
            while (eventReader.hasNext()) {
                event = eventReader.nextEvent();
                if (event.isEntityReference()) {
                    continue;
                }
                if (event.isStartElement()) {
                    startElement = event.asStartElement();
                    tagName = startElement.getName().getLocalPart();
                    if (tagName.equals("p")) {
                        attribute = startElement.getAttributeByName(new QName("class"));
                        if (attribute.getValue().equals("text-center")) {
                            while (!tagName.equals("a")) {
                                event = eventReader.nextEvent();
                                if (event.isStartElement()) {
                                    startElement = event.asStartElement();
                                    tagName = startElement.getName().getLocalPart();
                                }
                            }
                            link = startElement.getAttributeByName(new QName("href")).getValue();
                            linkList.add(link);
                        }

                    }
                }
            }
        } catch (Exception ex) {
            Logger.getLogger(FoodPageCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void run() {
        BufferedReader reader = null;
        try {
            reader = getBufferedReaderForURL(url);
            String line = "";
            String document = "";
            boolean isStart = false;
            while ((line = reader.readLine()) != null) {
                if (isStart && line.contains("<div class=\"thanhvien_page\">")) {
                    break;
                }
                if (isStart) {
                    if (line.contains("<a class=\"share_social_btn\"")) {
                        continue;
                    }
                    document += line.trim();
                }
                if (line.contains("<div class=\"tkmn_gr03 col-lg-12 col-md-12 col-sm-12 col-xs-12\">")) {
                    isStart = true;
                }
            }
            staxParserForEachPage(document);
            for (String link : linkList) {
                FoodDetailsCrawler foodDetailsCrawler = new FoodDetailsCrawler(link, category, this.getContext());
                foodDetailsCrawler.run();
            }

        } catch (Exception e) {
            Logger.getLogger(FoodPageCrawler.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (Exception e) {
                Logger.getLogger(FoodPageCrawler.class.getName()).log(Level.SEVERE, null, e);
            }
        }
    }
}
