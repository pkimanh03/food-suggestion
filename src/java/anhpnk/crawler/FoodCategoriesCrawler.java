/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.crawler;

import anhpnk.jaxb.foody.Category;
import anhpnk.utils.AppUtils;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 *
 * @author Kim Anh
 */
public class FoodCategoriesCrawler extends BaseCrawler {

    public FoodCategoriesCrawler(ServletContext context) {
        super(context);
    }

    private List<Category> staxParserForCategories(String document) {
        document = AppUtils.replaceInvalidCharacter(document);
        XMLEventReader eventReader;

        List<Category> result = new ArrayList<>();
        Category category = null;
        String name = "", link = "";

        StartElement startElement = null;
        String tagName = "";
        Attribute hrefAttribute = null;
        Characters nameCharacter = null;

        try {
            eventReader = parseStringToXMLEventReader(document);

            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                if (event.isStartElement()) {
                    startElement = event.asStartElement();
                    tagName = startElement.getName().getLocalPart();
                    if (tagName.equals("li")) {
                        event = eventReader.nextEvent();
                        hrefAttribute = event.asStartElement().getAttributeByName(new QName("href"));
                        link = hrefAttribute.getValue();

                        eventReader.nextEvent();
                        event = eventReader.nextEvent();
                        nameCharacter = event.asCharacters();
                        name = nameCharacter.getData().trim();

                        category = new Category();
                        category.setId(AppUtils.generateUUID().toString());
                        category.setName(name);
                        category.setLink(link);
                        result.add(category);
                    }
                }
            }

        } catch (Exception ex) {
            Logger.getLogger(FoodCategoriesCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public List<Category> getCategories(String url) {
        BufferedReader reader = null;
        List<Category> categoryList = null;
        try {
            reader = getBufferedReaderForURL(url);
            String line = "";
            String document = "";
            boolean isStart = false;

            while ((line = reader.readLine()) != null) {
                if (isStart && line.contains("food_group food_cookoccas")) {
                    break;
                }
                if (isStart) {
                    if (line.contains("<img")) {
                        String oldImgLine = line.substring(line.indexOf("<img"), line.indexOf(">", line.indexOf("<img")) + 1);
                        String newImgLine = oldImgLine.replaceAll(">", "/>");
                        line = line.replaceAll(oldImgLine, newImgLine);
                    }
                    document += line.trim();
                }
                if (line.contains("<div class=\"food_group food_howtocook\">")) {
                    isStart = true;
                    document += line.trim();
                }
            }
            categoryList = staxParserForCategories(document);
        } catch (Exception e) {
            Logger.getLogger(BaseCrawler.class.getName()).log(Level.SEVERE, null, e);
        }
        return categoryList;
    }
}
