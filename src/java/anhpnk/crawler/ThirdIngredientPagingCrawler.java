/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.crawler;

import anhpnk.jaxb.foody.Category;
import anhpnk.jaxb.foody.Food;
import anhpnk.utils.AppUtils;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 *
 * @author Kim Anh
 */
public class ThirdIngredientPagingCrawler extends BaseCrawler implements Runnable {

    public static List<Food> listAllFood = new ArrayList();
    private String url;

    public ThirdIngredientPagingCrawler(String url, ServletContext context) {
        super(context);
        this.url = url;
    }

    @Override
    public void run() {
        BufferedReader reader = null;
        try {
            reader = getBufferedReaderForURL(url);
            String line = "", document = "";
            boolean isStart = false;
            while ((line = reader.readLine()) != null) {
                if (isStart && line.contains("</div>")) {
                    document += line.trim();
//                    document += "</document>";
                    break;
                }
                if (isStart) {
                    document += line.trim();
                }
                if (line.contains("<div class=\"paging paging-bottom\">")) {
                    document += line.trim();
                    isStart = true;
                }
            }
            document = document.replaceAll("<div class=\"paging paging-top\"></div>", "");
            document = document.replaceAll("<div class=\"paging paging-bottom\"></div></div>", "");
            int numOfPage = getLastPage(document);
            for (int i = 1; i <= numOfPage; i++) {
                String pageURL = url + "?p=" + i;
                System.out.println("Page: " + pageURL);
                ThirdIngredientCrawler ingredientCrawler = new ThirdIngredientCrawler(pageURL, this.getContext());
                ingredientCrawler.run();
            }

        } catch (Exception e) {
            Logger.getLogger(FoodCrawler.class.getName()).log(Level.SEVERE, null, e);
        }
    }

    private int getLastPage(String document) {
        int pageCount = 0;
        try {
            document = AppUtils.replaceInvalidCharacter(document);
            XMLEventReader eventReader = parseStringToXMLEventReader(document);
            String tagName = "";
            Attribute attrPage = null;
            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                if (event.isStartElement()) {
                    StartElement startElement = event.asStartElement();
                    tagName = startElement.getName().getLocalPart();
                    if ("a".equals(tagName)) {
                        pageCount++;
                    }
                }
            }

        } catch (Exception ex) {
            Logger.getLogger(FoodCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return pageCount;
    }
}
