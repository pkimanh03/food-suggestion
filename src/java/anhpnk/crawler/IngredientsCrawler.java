/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.crawler;

import anhpnk.constant.AppConstant;
import anhpnk.daos.IngredientDAO;
import anhpnk.jaxb.ingredient.Ingredient;
import anhpnk.jaxb.ingredient.IngredientList;
import anhpnk.utils.AppUtils;
import java.io.BufferedReader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 *
 * @author Kim Anh
 */
public class IngredientsCrawler extends BaseCrawler implements Runnable {
    private String url;
    private IngredientList ingredientList;

    public IngredientsCrawler(ServletContext context, String url) {
        super(context);
        this.url = url;
        ingredientList = new IngredientList();
    }

    public IngredientList getIngredientList() {
        return ingredientList;
    }

    public void setIngredientList(IngredientList ingredientList) {
        this.ingredientList = ingredientList;
    }

    
    private List<String> staxParserForIngredientList(String document) {
        List<String> result = new ArrayList<>();
        try {
            document = document.trim();

            XMLEventReader eventReader = parseStringToXMLEventReader(document);

            StartElement startElement = null;
            String tagName = "";
            Characters character = null;
            Attribute rowspanAttribute = null;

            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                if (event.isStartElement()) {
                    startElement = event.asStartElement();
                    tagName = startElement.getName().getLocalPart();
                    rowspanAttribute = startElement.getAttributeByName(new QName("rowspan"));
                    if (tagName.equals("td") && rowspanAttribute == null) {
                        event = eventReader.nextEvent();
                        if (event.isCharacters()) {
                            character = event.asCharacters();
                            System.out.println("TD: " + character.getData());
                            result.add(character.getData().trim());
                        }

                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(IngredientsCrawler.class.getName()).log(Level.SEVERE, null, e);
        }
        return result;
    }

    private IngredientList convertToJAXBObject(List<String> parsedStringList) {
        IngredientList ingredientList = new IngredientList();
        String name = "";
        String unit = "";
        String price = "";
        
        Ingredient ingredient = null;
        for (int i = 0; i < parsedStringList.size(); i++) {
            String value = parsedStringList.get(i);
            if (value.matches("\\d*")) {
                System.out.println("NUMBER: " + value);
                name = parsedStringList.get(i + 1).trim();
                unit = parsedStringList.get(i + 2).trim();
                price = parsedStringList.get(i + 3).trim();
                
                ingredient = new Ingredient();
                ingredient.setId(AppUtils.generateUUID().toString());
                ingredient.setName(name);
                ingredient.setUnit(unit);
                ingredient.setPrice(new BigInteger(price.replaceAll("\\D", "").trim()));
                
                ingredientList.getIngredientItem().add(ingredient);
            }
        }
        return ingredientList;
    }
    
    @Override
    public void run() {
        BufferedReader reader = null;
        try {
            reader = getBufferedReaderForURL(url);

            String line = "";
            String document = "";
            List<String> documentList = new ArrayList<>();
            boolean isStart = false;
            boolean isFound = false;

            while ((line = reader.readLine()) != null) {
                if (isStart && line.contains("</table>")) {
                    documentList.add(document);
                    document = "";
                    isFound = false;
                    isStart = false;
                }
                if (isStart) {
                    document += line.trim();
                }
                if (isFound && line.contains("<table")) {
                    isStart = true;
                }
                if (line.contains("<div class=\"su-tabs-pane su-clearfix\"")) {
                    isFound = true;
                }
            }
            
            
            for (String doc : documentList) {
                List<String> parseResult = staxParserForIngredientList(doc);
                IngredientList parseToIngredient = convertToJAXBObject(parseResult);
                this.getIngredientList().getIngredientItem().addAll((Collection<? extends Ingredient>) parseToIngredient.getIngredientItem());
            }
            AppUtils.saveToXML(AppConstant.INGREDIENT_XML_FILE_PATH, ingredientList);
            boolean isValid = AppUtils.validateBeforeSaveToDB(AppConstant.INGREDIENT_SCHEMA_FILE_PATH, AppConstant.INGREDIENT_XML_FILE_PATH, ingredientList);
            if (isValid) {
                IngredientDAO dao = IngredientDAO.getInstance();
                anhpnk.entity.Ingredient entity = null;
                for (Ingredient ingredient : this.ingredientList.getIngredientItem()) {
                    entity = new anhpnk.entity.Ingredient(ingredient);
                    dao.insert(entity);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(IngredientsCrawler.class.getName()).log(Level.SEVERE, null, e);
        }
    }

}
