/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.crawler;

import java.io.BufferedReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 *
 * @author Kim Anh
 */
public class IngredientLinkCrawler extends BaseCrawler {

    public IngredientLinkCrawler(ServletContext context) {
        super(context);
    }

    private String staxParserForIngredientCategory(String document) {
        document = document.trim();
        
        String link = "";
        try {
            XMLEventReader eventReader = parseStringToXMLEventReader(document);
            
            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                System.out.println("Event "+event);
                if (event.isStartElement()) {
                    StartElement startElement = event.asStartElement();
                    String tagName = startElement.getName().getLocalPart();
                    if (tagName.equals("a")) {
                        Attribute attribute = startElement.getAttributeByName(new QName("href"));
                        if (attribute.getValue().trim().equals("http://tmquoctuan.com/bang-bao-gia-rau-cu-qua/")) {
                            link = attribute.getValue().trim();
                            break;
                        }
                    }
                }
            }
            return link;
        } catch (Exception e) {
            Logger.getLogger(IngredientLinkCrawler.class.getName()).log(Level.SEVERE, null, e);
        }
        return link;
    }
    public String getLinkIngredient(String url) {
        String result = "";

        BufferedReader reader = null;
        try {
            reader = getBufferedReaderForURL(url);
            
            String line = "";
            String document = "<document>";
            boolean isFound = false;
            boolean isStart = false;
            
            while ((line = reader.readLine()) != null) {
                if (isStart && line.contains("</ul>")) {
                    break;
                }
                if (isStart) {
                    document += line.trim();
                }
                if (isFound && line.contains("<ul id=\"menu-main-menu\"")) {
                    isStart = true;
                }
                if (line.contains("<div class=\"header-link-left\">")) {
                    isFound = true;
                }
            }
            document += "</document>";
            result = staxParserForIngredientCategory(document);
        } catch (Exception e) {
            Logger.getLogger(IngredientLinkCrawler.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
            } catch (Exception e) {
                Logger.getLogger(IngredientLinkCrawler.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return result;
    }
}
