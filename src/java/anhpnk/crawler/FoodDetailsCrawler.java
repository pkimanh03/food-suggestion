/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.crawler;

import anhpnk.constant.AppConstant;
import anhpnk.daos.FoodDAO;
import anhpnk.jaxb.foody.Category;
import anhpnk.jaxb.foody.Food;
import anhpnk.jaxb.foody.IngredientFood;
import anhpnk.jaxb.foody.IngredientList;
import anhpnk.utils.AppUtils;
import java.io.BufferedReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.servlet.ServletContext;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 *
 * @author Kim Anh
 */
public class FoodDetailsCrawler extends BaseCrawler {

    private String url;
    private Category category;

    public FoodDetailsCrawler(String url, Category category, ServletContext context) {
        super(context);
        this.url = url;
        this.category = category;
    }

    private Map<String, String> getDocument() {
        Map<String, String> document = new HashMap();
        String foodDocument = "";
        String ingredientDocument = "<document>";
        BufferedReader reader = null;
        String line;
        boolean isStartFood = false;
        boolean isFoundIngredient = false;
        boolean isStartIngredient = false;
        try {
            reader = getBufferedReaderForURL(url);
            while ((line = reader.readLine()) != null) {
                if (isStartIngredient && line.contains("<li class=\"giavi\">")) {
                    ingredientDocument += "</document>";
                    break;
                }
                if (isStartIngredient) {
                    if (line.contains("<img")) {
                        String oldImgLine = line.substring(line.indexOf("<img"), line.indexOf(">", line.indexOf("<img")) + 1);
                        String newImgLine = oldImgLine.replaceAll(">", "></img>");
                        line = line.replaceAll(oldImgLine, newImgLine);
                    }
                    ingredientDocument += line.trim();
                }
                if (isFoundIngredient && line.contains("<ul>")) {
                    isStartIngredient = true;
                }
                if (isStartFood && line.contains("<div class=\"detail_info\">")) {
                    isFoundIngredient = true;
                    isStartFood = false;
                }
                if (isStartFood) {
                    if (line.contains("<img")) {
                        String oldImgLine = line.substring(line.indexOf("<img"), line.indexOf(">", line.indexOf("<img")) + 1);
                        String newImgLine = oldImgLine.replaceAll(">", "></img>");
                        line = line.replaceAll(oldImgLine, newImgLine);
                    }
                    foodDocument += line.trim();
                }
                if (line.contains("<div class=\"clearfix\">")) {
                    foodDocument += line.trim();
                    isStartFood = true;
                }
            }
            document.put("FOOD", foodDocument);
            document.put("INGREDIENT", ingredientDocument);
        } catch (Exception e) {
            Logger.getLogger(FoodPageCrawler.class.getName()).log(Level.SEVERE, null, e);
        }
        return document;
    }

    private Food staxParserForGetFood(String document) {
        Food food = new Food();
        String img = "", name = "", quantity = "";
        try {
            document = AppUtils.replaceInvalidCharacter(document);
            XMLEventReader eventReader = parseStringToXMLEventReader(document);
            XMLEvent event = null;
            StartElement startElement = null;
            Attribute attribute = null;
            Characters characters = null;
            String tagName = "";
            food.setId(AppUtils.generateUUID().toString());
            food.setCategory(category);
            food.setLink(url);
            food.setImg(img);
            food.setName(name);
            food.setQuantity(AppConstant.DEFAULT_QUANTITY);

            while (eventReader.hasNext()) {
                event = eventReader.nextEvent();
                if (event.isStartElement()) {
                    startElement = event.asStartElement();
                    tagName = startElement.getName().getLocalPart();
                    if (tagName.equals("img")) {
                        attribute = startElement.getAttributeByName(new QName("src"));
                        img = attribute.getValue();
                        food.setImg(img);

                        attribute = startElement.getAttributeByName(new QName("alt"));
                        name = attribute.getValue();
                        food.setName(name);
                    }
                    if (tagName.equals("h1")) {
                        event = eventReader.nextEvent();
                        characters = event.asCharacters();
                        name = characters.getData();
                        food.setName(name);
                    }
                    if (tagName.equals("span")) {
                        event = eventReader.nextEvent();
                        characters = event.asCharacters();
                        quantity = characters.getData().replaceAll("\\D", "").trim();
                        if (quantity == null || quantity.equals("") || quantity.isEmpty()) quantity = "1";
                        food.setQuantity(quantity);
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(FoodPageCrawler.class.getName()).log(Level.SEVERE, null, e);
        }
        return food;
    }

    private List<String> staxParserForGetIngredientList(String document) {
        List<String> ingredientList = new ArrayList<>();
        try {
            document = AppUtils.replaceInvalidCharacter(document);
            XMLEventReader eventReader = parseStringToXMLEventReader(document);
            XMLEvent event = null;
            StartElement startElement = null;
            Characters characters = null;
            String tagName = "";

            while (eventReader.hasNext()) {
                event = eventReader.nextEvent();
                if (event.isStartElement()) {
                    startElement = event.asStartElement();
                    tagName = startElement.getName().getLocalPart();
                    if (tagName.equals("span")) {
                        event = eventReader.nextEvent();
                        if (event.isCharacters()) {
                            characters = event.asCharacters();
                            ingredientList.add(characters.getData().trim());
                        }
                    }
                }
            }
        } catch (Exception e) {
            Logger.getLogger(FoodPageCrawler.class.getName()).log(Level.SEVERE, null, e);
        }
        return ingredientList;
    }

    private IngredientList convertToIngredientFood(List<String> ingredientStringList, Food food) {
        IngredientList ingredientList = new IngredientList();
        Pattern pattern = Pattern.compile(AppConstant.QUANTITY_REGEX_PATTERN);
        Pattern patternQuantity = Pattern.compile(AppConstant.QUANTITY_REGEX);
        Matcher matcher = null;
        Matcher matcherQuantity = null;
        IngredientFood ingredientFood = null;
        List<IngredientFood> result = new ArrayList<>();

        String tmp = "";
        String name = "";
        String quantity = "";
        for (String ingredientString : ingredientStringList) {
            ingredientString = ingredientString.trim();
            ingredientString = ingredientString.replaceAll("\\)", "").trim();
            ingredientString = ingredientString.replaceAll("\\(", "").trim();
            ingredientString = ingredientString.replaceAll(":", "").trim();

            matcher = pattern.matcher(ingredientString);
            if (matcher.find()) {
                matcherQuantity = patternQuantity.matcher(ingredientString);
                int start = -1;
                if (matcherQuantity.find()) {
                    start = matcherQuantity.start();
                }
                if (start >= 0) {
                    quantity = ingredientString.substring(start);
                }
                name = ingredientString.replaceAll(quantity, "").trim();
                ingredientFood = new IngredientFood();
                ingredientFood.setId(AppUtils.generateUUID().toString());
                ingredientFood.setQuantity(quantity);
                ingredientFood.setName(name);
                result.add(ingredientFood);
            } else {
                quantity = 0 + "";
                name = ingredientString;
                ingredientFood = new IngredientFood();
                ingredientFood.setId(AppUtils.generateUUID().toString());
                ingredientFood.setQuantity(quantity);
                ingredientFood.setName(name);
                result.add(ingredientFood);
            }

        }
        ingredientList.getIngredient().addAll(result);
        return ingredientList;
    }

    public synchronized void run() {
        Map<String, String> document = getDocument();
        String foodDoc = AppUtils.replaceInvalidCharacter(document.get("FOOD"));
        String ingredientDoc = AppUtils.replaceInvalidCharacter(document.get("INGREDIENT"));
        Food food = staxParserForGetFood(foodDoc);
        List<String> ingredientStringList = staxParserForGetIngredientList(ingredientDoc);
        IngredientList ingredientList = convertToIngredientFood(ingredientStringList, food);
        food.setIngredientList(ingredientList);
        
        AppUtils.saveToXML(AppConstant.FOOD_XML_FILE_PATH, food);
        boolean isValid = AppUtils.validateBeforeSaveToDB(AppConstant.FOOD_SCHEMA_FILE_PATH, AppConstant.FOOD_XML_FILE_PATH, food);
        System.out.println("VALID: " + isValid);
        if (isValid) {
            FoodDAO dao = FoodDAO.getInstance();
            boolean inserted = dao.insertCompleteFood(food);
            System.out.println("INSERT: " + inserted);
        }
    }
}
