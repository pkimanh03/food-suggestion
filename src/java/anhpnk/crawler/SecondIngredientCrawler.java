/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.crawler;

import anhpnk.constant.AppConstant;
import anhpnk.daos.IngredientDAO;
import anhpnk.jaxb.ingredient.Ingredient;
import anhpnk.jaxb.ingredient.IngredientList;
import anhpnk.utils.AppUtils;
import java.io.BufferedReader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 *
 * @author Kim Anh
 */
public class SecondIngredientCrawler extends BaseCrawler implements Runnable {

    private String url;
    private IngredientList ingredientList;

    public SecondIngredientCrawler(ServletContext context, String url) {
        super(context);
        this.url = url;
        ingredientList = new IngredientList();
    }

    public IngredientList getIngredientList() {
        return ingredientList;
    }

    public void setIngredientList(IngredientList ingredientList) {
        this.ingredientList = ingredientList;
    }

    private List<Ingredient> staxParserForIngredientList(String document) {
        List<Ingredient> result = new ArrayList();
        Ingredient ingredient = null;
        document = document.trim();
        XMLEventReader eventReader;
        String name = "", unit = "", price = "";

        StartElement startElement = null;
        String tagName = "";
        Attribute attribute = null;
        try {
            eventReader = parseStringToXMLEventReader(document);
            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                if (event.isStartElement()) {
                    startElement = event.asStartElement();
                    tagName = startElement.getName().getLocalPart();
                    if (tagName.equals("a")) {
                        attribute = startElement.getAttributeByName(new QName("data-unit"));
                        unit = attribute.getValue();
                        attribute = startElement.getAttributeByName(new QName("data-name"));
                        name = attribute.getValue();
                        attribute = startElement.getAttributeByName(new QName("data-price"));
                        price = attribute.getValue().replaceAll("\\D", "").trim();
                        System.out.println(name + "-" + price + "-" + unit);

                        BigInteger bi = new BigInteger(price);
                        if (bi.intValue() > 0) {
                            ingredient = new Ingredient();
                            ingredient.setId(AppUtils.generateUUID().toString());
                            ingredient.setName(name);
                            ingredient.setPrice(new BigInteger(price));
                            ingredient.setUnit(unit);
                            result.add(ingredient);
                        }

                    }
                }
            }

        } catch (Exception ex) {
            Logger.getLogger(FoodCategoriesCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    @Override
    public void run() {
        BufferedReader reader = null;
        try {
            reader = getBufferedReaderForURL(url);

            String line = "";
            String document = "<document>";

            while ((line = reader.readLine()) != null) {
                if (line.contains("<a data-product-id=")) {
                    document += line.trim();
                }
            }
            document = document.replaceAll("</div>", "");
            document += "</document>";
            List<Ingredient> list = staxParserForIngredientList(document);
            ingredientList.getIngredientItem().addAll(list);
            AppUtils.saveToXML(AppConstant.INGREDIENT_XML_FILE_PATH, ingredientList);
            boolean isValid = AppUtils.validateBeforeSaveToDB(AppConstant.INGREDIENT_SCHEMA_FILE_PATH, AppConstant.INGREDIENT_XML_FILE_PATH, ingredientList);
            if (isValid) {
                IngredientDAO dao = IngredientDAO.getInstance();
                anhpnk.entity.Ingredient entity = null;
                for (Ingredient ingredient : this.ingredientList.getIngredientItem()) {
                    entity = new anhpnk.entity.Ingredient(ingredient);
                    dao.insert(entity);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(IngredientsCrawler.class.getName()).log(Level.SEVERE, null, e);
        }
    }

}
