/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.crawler;

import anhpnk.constant.AppConstant;
import anhpnk.daos.IngredientDAO;
import static anhpnk.entity.Recipe_.category;
import anhpnk.jaxb.ingredient.Ingredient;
import anhpnk.jaxb.ingredient.IngredientList;
import anhpnk.utils.AppUtils;
import java.io.BufferedReader;
import java.math.BigInteger;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 *
 * @author Kim Anh
 */
public class ThirdIngredientCrawler extends BaseCrawler {

    private String url;

    public ThirdIngredientCrawler(String url, ServletContext context) {
        super(context);
        this.url = url;
    }

    private String getDocument() {
        String document = "<document>";
        BufferedReader reader = null;
        String line;
        boolean isStart = false;
        try {
            reader = getBufferedReaderForURL(url);
            while ((line = reader.readLine()) != null) {
                if (isStart && line.contains("class=\"paging paging-bottom\"")) {
                    document += "</document>";
                    break;
                }
                if (isStart) {
                    document += line.trim();
                }
                if (line.contains("class=\"datalist-content\"")) {
                    document += line.trim();
                    isStart = true;
                }
            }
        } catch (Exception e) {
            Logger.getLogger(FoodPageCrawler.class.getName()).log(Level.SEVERE, null, e);
        }
        return document;
    }

    private List<Ingredient> staxParser(String document) {
        List<Ingredient> result = new ArrayList<>();
        Ingredient i = null;
        String name, unit, quantity, priceStr;
        double price = 0;
        try {
            document = document.trim().replaceAll("  ", "").replaceAll("> <", "><");
            XMLEventReader eventReader = parseStringToXMLEventReader(document);
            XMLEvent event;
            StartElement startElement;
            Attribute attribute;
            Characters characters;
            String tagName;

            while (eventReader.hasNext()) {
                event = eventReader.nextEvent();
                System.out.println("Event "+event);
//                if (event.isStartElement()) {
//                    startElement = event.asStartElement();
//                    tagName = startElement.getName().getLocalPart();
//                    if (tagName.equals("div")) {
//                        attribute = startElement.getAttributeByName(new QName("class"));
//                        if (attribute.getValue().equals("product-item")) {
//                            i = new Ingredient();
//                            i.setId(AppUtils.generateUUID().toString());
//                            //get ingredient name
//                            while (!attribute.getValue().equals("name")) {
//                                event = eventReader.nextEvent();
//                                if (event.isStartElement()) {
//                                    startElement = event.asStartElement();
//                                    attribute = startElement.getAttributeByName(new QName("class"));
//                                }
//                            }
//                            event = eventReader.nextEvent();
//                            if (event.isCharacters()) {
//                                name = event.asCharacters().getData().trim();
//                                i.setName(name);
//                                System.out.println("NAME: " + name);
//                            }
//                            //get ingredient unit
//                            while (!attribute.getValue().equals("unit")) {
//                                event = eventReader.nextEvent();
//                                if (event.isStartElement()) {
//                                    startElement = event.asStartElement();
//                                    attribute = startElement.getAttributeByName(new QName("class"));
//                                }
//                            }
//                            event = eventReader.nextEvent();
//                            if (event.isCharacters()) {
//                                characters = event.asCharacters();
//                                if (characters.getData().trim().contains("/")) {
//                                    unit = characters.getData().split("/")[0];
//                                    quantity = unit.replaceAll("\\D", "");
//                                } else {
//                                    unit = characters.getData().trim();
//                                    quantity = unit.replace("\\D", "");
//                                    if (quantity.equals("")) {
//                                        quantity = "1";
//                                    }
//                                }
//                                i.setUnit(unit);
//                                System.out.println("UNIT: " + unit);
//                            }
//                            //get ingredient price
//                            while (!attribute.getValue().equals("price-new")) {
//                                event = eventReader.nextEvent();
//                                if (event.isStartElement()) {
//                                    startElement = event.asStartElement();
//                                    attribute = startElement.getAttributeByName(new QName("class"));
//                                }
//                            }
//                            event = eventReader.nextEvent();
//
//                            if (event.isCharacters()) {
//                                priceStr = event.asCharacters().getData().trim().replaceAll("\\D", "");
//                                i.setPrice(new BigInteger(priceStr));
//                                System.out.println("PRICE: " + price);
//                            }
//                            System.out.println("I: " + i);
//                            result.add(i);
//                        }
//                    }
//                }
            }
        } catch (Exception e) {
            Logger.getLogger(FoodPageCrawler.class.getName()).log(Level.SEVERE, null, e);
        }
        return result;
    }

    private IngredientList convertToIngredientFood(List<Ingredient> ingredients) {
        IngredientList ingredientList = new IngredientList();

        String unit = "", quantity = "", tmp = "";
        for (Ingredient ingredient : ingredients) {
            tmp = ingredient.getUnit();
            if (tmp.contains("kg")) {
                ingredient.setUnit("kg");
                quantity = tmp.replaceAll("kg", "");
                if (!quantity.equals("")) {
                    String[] split = quantity.split("[,.]");
                    ingredient.setPrice(ingredient.getPrice().multiply(new BigInteger(split[0])));
                }
            } else if (tmp.contains("gr")) {
                ingredient.setUnit("kg");
                quantity = tmp.replaceAll("\\D", "");
                int k = 1000 / Integer.parseInt(quantity);
                ingredient.setPrice(ingredient.getPrice().multiply(new BigInteger(k + "")));
            } else {
                ingredient.setUnit(tmp.replaceAll("\\d", ""));
            }
            System.out.println("JAXB: " + ingredient);
        }
        ingredientList.getIngredientItem().addAll(ingredients);
        return ingredientList;
    }

    public synchronized void run() {
        String document = getDocument().trim().replaceAll("  ", "").replaceAll("> <", "><");
        document = AppUtils.replaceInvalidCharacter(document);
        List<Ingredient> ingredients = staxParser(document);
//        IngredientList il = convertToIngredientFood(ingredients);
//        AppUtils.saveToXML(AppConstant.PROJECT_PATH + LocalDate.now() + ".xml", il);
//        boolean isValid = AppUtils.validateBeforeSaveToDB(AppConstant.INGREDIENT_SCHEMA_FILE_PATH, AppConstant.INGREDIENT_XML_FILE_PATH, il);
//        System.out.println("VALID: " + isValid);
//        if (isValid) {
//            IngredientDAO dao = IngredientDAO.getInstance();
//            anhpnk.entity.Ingredient entity = null;
//            for (Ingredient ingredient : il.getIngredientItem()) {
//                entity = new anhpnk.entity.Ingredient(ingredient);
////                dao.insert(entity);
//                System.out.println("ENTITY: " + entity);
//            }
//        }
    }
}
