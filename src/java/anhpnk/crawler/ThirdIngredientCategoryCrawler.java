/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.crawler;

import anhpnk.constant.AppConstant;
import anhpnk.daos.IngredientDAO;
import anhpnk.jaxb.ingredient.Ingredient;
import anhpnk.jaxb.ingredient.IngredientList;
import anhpnk.utils.AppUtils;
import java.io.BufferedReader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;
import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;

/**
 *
 * @author Kim Anh
 */
public class ThirdIngredientCategoryCrawler extends BaseCrawler {

    public ThirdIngredientCategoryCrawler(ServletContext context) {
        super(context);
    }

    private List<Ingredient> staxParserForCategories(String document) {
        document = AppUtils.replaceInvalidCharacter(document);
        XMLEventReader eventReader;

        List<Ingredient> result = new ArrayList<>();
        Ingredient i = null;

        StartElement startElement = null;
        String tagName = "";
        Attribute attribute = null;

        String name, priceStr, unit;
        try {
            eventReader = parseStringToXMLEventReader(document);

            while (eventReader.hasNext()) {
                XMLEvent event = eventReader.nextEvent();
                if (event.isStartElement()) {
                    startElement = event.asStartElement();
                    tagName = startElement.getName().getLocalPart();
                    if (tagName.equals("div")) {
                        attribute = startElement.getAttributeByName(new QName("class"));
                        if (attribute != null && attribute.getValue().equals("product-info")) {
                            i = new Ingredient();
                            i.setId(AppUtils.generateUUID().toString());
                            i.setUnit("kg");
                            while (attribute != null && !attribute.getValue().equals("product-name")) {
                                event = eventReader.nextEvent();
                                if (event.isStartElement()) {
                                    startElement = event.asStartElement();
                                    attribute = startElement.getAttributeByName(new QName("class"));
                                }
                            }
                            attribute = startElement.getAttributeByName(new QName("title"));
                            name = attribute.getValue().trim();
                            i.setName(name);
                            
                            while (attribute != null && !tagName.equals("span") && !attribute.getValue().equals("mobile")) {
                                event = eventReader.nextEvent();
                                if (event.isStartElement()) {
                                    startElement = event.asStartElement();
                                    tagName = startElement.getName().getLocalPart();
                                    attribute = startElement.getAttributeByName(new QName("class"));
                                }
                            }
                            while (!event.isCharacters()) {
                                event = eventReader.nextEvent();
                            }
                            priceStr = event.asCharacters().getData().trim().replaceAll("\\D", "");
                            i.setPrice(new BigInteger(priceStr));
                            result.add(i);
                        }
                    }
                }
            }

        } catch (Exception ex) {
            Logger.getLogger(FoodCategoriesCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    public List<String> getCategories(String url) {
        BufferedReader reader = null;
        List<String> categoryList = null;
        try {
            reader = getBufferedReaderForURL(url);
            String line = "";
            String document = "";
            boolean isStart = false;

            while ((line = reader.readLine()) != null) {
                if (isStart && line.contains("</section>")) {
                    document += line.trim();
                    break;
                }
                if (isStart) {
                    document += line.trim();
                }
                if (line.contains("m-content")) {
                    document += line.trim();
                    isStart = true;
                }
            }
            document = document.replace("</aside>", "");
            List<Ingredient> parse = staxParserForCategories(document);
            System.out.println("SIZE: " + parse.size());
            IngredientList il = new IngredientList();
            il.getIngredientItem().addAll(parse);
            AppUtils.saveToXML(AppConstant.INGREDIENT_XML_FILE_PATH, il);
            boolean isValid = AppUtils.validateBeforeSaveToDB(AppConstant.INGREDIENT_SCHEMA_FILE_PATH, AppConstant.INGREDIENT_XML_FILE_PATH, il);
            if (isValid) {
                IngredientDAO dao = IngredientDAO.getInstance();
                anhpnk.entity.Ingredient entity = null;
                for (Ingredient ingredient : il.getIngredientItem()) {
                    entity = new anhpnk.entity.Ingredient(ingredient);
                    dao.insert(entity);
                }
            }
        } catch (Exception e) {
            Logger.getLogger(BaseCrawler.class.getName()).log(Level.SEVERE, null, e);
        }
        return categoryList;
    }
}
