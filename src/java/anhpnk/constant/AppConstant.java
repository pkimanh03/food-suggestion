/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.constant;

/**
 *
 * @author Kim Anh
 */
public class AppConstant {

    public static final int CRAWLING_BREAK_TIME = 10000;
    public static final String ENTITY_MANAGER_FACTORY_NAME = "FoodSuggestion";
    public static final String MULTIPLE = "MULTIPLE";
    public static final String SINGLE = "SINGLE";
    public static final String PROJECT_PATH = "E:\\Food\\web\\WEB-INF\\";
    public static final String QUANTITY_REGEX = "\\d{1,3}";
    public static final String UNIT_QUANTITY_REGEX = "\\d{1,3}[gr|g]";
    public static final String QUANTITY_REGEX_PATTERN = ".*\\d.*";
    public static final String DEFAULT_QUANTITY = "1";
    public static final String FOOD_SCHEMA_FILE_PATH = PROJECT_PATH + "foody.xsd";
    public static final String FOOD_XML_FILE_PATH = PROJECT_PATH + "foody.xml";
    public static final String INGREDIENT_SCHEMA_FILE_PATH = AppConstant.PROJECT_PATH + "ingredient.xsd";
    public static final String INGREDIENT_XML_FILE_PATH = AppConstant.PROJECT_PATH + "ingredient.xml";
    public static final String XSL_FILE_PATH = "E:\\Food\\src\\java\\anhpnk.xsl\\IngredientPDF.xsl";
    public static final String FOOD_PDF_XML_FILE_PATH = PROJECT_PATH + "food_pdf.xml";
    
}
