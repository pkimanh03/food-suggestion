/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.constant;

/**
 *
 * @author Kim Anh
 */
public class DictionaryConstant {
    
    public static final double BASED_QUANTITY = 0.1;
    
//    public static final String GA = "gà";
//    public static final String VIT = "vịt";
//    public static final String BO = "bò";
//    public static final String HEO = "heo";
//    public static final String LON = "lợn";
//    public static final String TOM = "tôm";
//    public static final String MUC = "mực";
//    public static final String CA = "cá";
//    public static final String TRUNG = "trứng";
//    public static final String RAU = "rau";
//    public static final String HANH = "hành";
//    public static final String CAF = "cà";
//    public static final String CAI = "cải";
//    public static final String CAM = "cam";
//    public static final String CHANH = "chanh";
//    public static final String OT = "ớt";
//    public static final String NAM = "nấm";
//    public static final String CHUOI = "chuối";
//    public static final String BI = "bí";
//    public static final String DAU = "đậu";
//    public static final String SU = "su";
//    public static final String KHOAI = "khoai";
//    public static final String DUAF = "dừa";
//    public static final String DUA = "dưa";
//    public static final String NUOC = "nước";
//    public static final String BOT = "bột";
//    public static final String THOM = "thơm";
//    public static final String CHA = "chả";
//    public static final String GIA = "giá";
//    public static final String MIT = "mít";
//    public static final String TAC = "tắc";
//    public static final String BACH_TUOC = "bạch tuộc";
//    public static final String OTHER = "other";
    
    public static final String MON_XAO = "xào";
    public static final String MON_OM = "om";
    public static final String MON_KHO = "kho";
    public static final String MON_CANH = "canh";
    public static final String MON_CHIEN = "chiên";

    public static final String[] MAIN_CATEGORY = {"canh", "kho", "xào", "om", "chiên"};
    public static final String[] RECIPE_CATEGORY = {
        "canh-kho-xào",
        "canh-kho-om",
        "canh-kho-chiên",
        "canh-xào-om",
        "canh-xào-chiên",
        "canh-om-chiên",
        "kho-xào-om",
        "kho-xào-chiên",
        "kho-om-chiên",
        "om-chiên-kho"
    };

    public static final String[] CHIEN_EXCEPTION = {"bánh", "pancake", "spaghetti", "mì", "burger", "pizza", "cơm", "bột chiên", "sanwich", "miến", "bún", "salad", "gỏi", "chả", "xôi", "viên"};
    public static final String[] XAO_EXCEPTION = {"hủ tiếu", "mì", "cơm", "bánh", "bún", "miến", "phở", "nui", "pasta", "pizza", "gnocchi", "pad", "salad"};
    public static final String[] KHO_EXCEPTION = {"hấp", "bò kho"};
    public static final String[] CANH_EXCEPTION = {"bánh", "súp", "cháo", "hủ tiếu", "bún", "phở"};
    
    public static final String[] FOOD_ADJ = {"[[xốt]\\D.]", "ngũ vị", "giòn", "chay", "một nắng", "[[vị]\\D.]", "xiên que", "cuộn", "cay", "chua ngọt", "thập cẩm", "khìa", "ngũ sắc", "kiểu hàn quốc", "lăn"};
    public static final String[] REJECT_UNIT = {"l" ,"ít","gói","m","chai","tép","quả","trái"};
    
}
