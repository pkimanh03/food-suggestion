/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.controller;

import anhpnk.daos.FoodDAO;
import anhpnk.entity.Food;
import anhpnk.entity.Recipe;
import anhpnk.utils.SuggestionUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Kim Anh
 */
public class SuggestDetailsController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String recipeId = request.getParameter("id");
            String recipeName = request.getParameter("name");
            String recipePrice = request.getParameter("price");
            String recipeCategory = request.getParameter("category");
            
            Recipe recipe = new Recipe(recipeId, recipeName, recipeCategory, 0, Double.parseDouble(recipePrice));
            request.setAttribute("RECIPE", recipe);
            String fid1 = request.getParameter("fid1");
            String fid2 = request.getParameter("fid2");
            String fid3 = request.getParameter("fid3");
            FoodDAO fDao = FoodDAO.getInstance();
            Food f1 = fDao.getById(fid1);
            Food f2 = fDao.getById(fid2);
            Food f3 = fDao.getById(fid3);
            
            String peopleStr = request.getParameter("numOfPeople");
            int people = Integer.parseInt(peopleStr);
//            f1 = SuggestionUtils.calculateQuantity(f1, people);
//            f2 = SuggestionUtils.calculateQuantity(f2, people);
//            f3 = SuggestionUtils.calculateQuantity(f3, people);
            request.setAttribute("F1", f1);
            request.setAttribute("F2", f2);
            request.setAttribute("F3", f3);
        } catch (Exception e) {
            Logger.getLogger(SuggestDetailsController.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            request.getRequestDispatcher("recipe_suggestion.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
