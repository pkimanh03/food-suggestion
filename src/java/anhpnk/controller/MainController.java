/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.controller;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Kim Anh
 */
public class MainController extends HttpServlet {

    private final String ERROR = "error.jsp";
    private final String SUGGEST = "SuggestController";
    private final String SUGGEST_DETAILS = "SuggestDetailsController";
    private final String RECIPE_DETAILS = "RecipeDetailsController";
    private final String HOME = "HomeController";
    private final String SATISFY = "RateController";
    private final String LOGIN = "LoginController";
    private final String FAVORITE = "FavoriteController";
    private final String SAVE_RECIPE = "SaveRecipeController";
    private final String CRAWLER = "CrawlerController";
    private final String PRINT_PDF = "PrintPDFController";
    private final String LOG_OUT = "LogOutController";
    private final String REGISTRATION = "UserRegistrationController";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;
        try {
            System.out.println("MainController");
            String button = request.getParameter("button");
            if (button == null) {
                button = "";
            }
            switch (button) {
                case "btnSuggest":
                    url = SUGGEST;
                    break;
                case "btnSuggestDetails":
                    url = SUGGEST_DETAILS;
                    break;
                case "btnSaveRecipe":
                    url = SAVE_RECIPE;
                    break;
                case "btnRecipeDetails":
                    url = RECIPE_DETAILS;
                    break;
                case "btnLike":
                    url = SATISFY;
                    break;
                case "btnLogin":
                    url = LOGIN;
                    break;
                case "btnCrawler":
                    url = CRAWLER;
                    break;
                case "btnHome":
                    url = HOME;
                    break;
                case "btnFavorite":
                    url = FAVORITE;
                    break;
                case "btnPrintPDF":
                    url = PRINT_PDF;
                    break;
                case "btnRegistration":
                    url = REGISTRATION;
                    break;
                case "btnLogOut":
                    url = LOG_OUT;
                    break;
                default:
                    url = HOME;
                    break;
            }
        } catch (Exception e) {
            Logger.getLogger(MainController.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
