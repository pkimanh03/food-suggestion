/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.controller;

import anhpnk.constant.AppConstant;
import anhpnk.daos.FoodDAO;
import anhpnk.entity.Food;
import anhpnk.entity.FoodIngredient;
import anhpnk.jaxb.foody.IngredientFood;
import anhpnk.jaxb.foody.IngredientList;
import anhpnk.utils.AppUtils;
import anhpnk.utils.PDFUtils;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamSource;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;

/**
 *
 * @author Kim Anh
 */
public class PrintPDFController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String id = request.getParameter("id");
            FoodDAO dao = FoodDAO.getInstance();
            Food f = dao.getById(id);
            List<IngredientFood> ingredientsJAXB = new ArrayList<>();
            IngredientList il = new IngredientList();
            for (FoodIngredient fiEntity : f.getFoodIngredientCollection()) {
                IngredientFood jaxb = new IngredientFood();
                jaxb.setId(fiEntity.getId());
                jaxb.setName(fiEntity.getName());
                jaxb.setQuantity(fiEntity.getQuantity());
                ingredientsJAXB.add(jaxb);
                il.getIngredient().add(jaxb);
            }

            AppUtils.saveToXML(AppConstant.FOOD_PDF_XML_FILE_PATH, il);
            File xsltFile = new File(AppConstant.XSL_FILE_PATH);
            StreamSource xmlSource = new StreamSource(new File(AppConstant.FOOD_PDF_XML_FILE_PATH));
            FopFactory fopFactory = FopFactory.newInstance();
            FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
            OutputStream out;
            out = new java.io.FileOutputStream(AppConstant.PROJECT_PATH + "\\food.pdf");

            try {
                Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);
                TransformerFactory factory = TransformerFactory.newInstance();
                Transformer transformer = factory.newTransformer(new StreamSource(xsltFile));
                Result res = new SAXResult(fop.getDefaultHandler());
                transformer.transform(xmlSource, res);
            } finally {
                out.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
// <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
        /**
         * Handles the HTTP <code>GET</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doGet
        (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            processRequest(request, response);
        }

        /**
         * Handles the HTTP <code>POST</code> method.
         *
         * @param request servlet request
         * @param response servlet response
         * @throws ServletException if a servlet-specific error occurs
         * @throws IOException if an I/O error occurs
         */
        @Override
        protected void doPost
        (HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            processRequest(request, response);
        }

        /**
         * Returns a short description of the servlet.
         *
         * @return a String containing servlet description
         */
        @Override
        public String getServletInfo
        
        
        
        
            () {
        return "Short description";
        }// </editor-fold>

    }
