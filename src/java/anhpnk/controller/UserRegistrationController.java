/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.controller;

import anhpnk.daos.AccountDAO;
import anhpnk.entity.Account;
import anhpnk.service.AccountClient;
import anhpnk.utils.AppUtils;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Kim Anh
 */
public class UserRegistrationController extends HttpServlet {

    private final String ERROR = "error.jsp";
    private final String LOGIN = "LoginController";
    private final String REJECT = "homepage_header.jsp";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        String url = ERROR;

        String username = request.getParameter("username");
        String password = request.getParameter("password");
        try {
            String fullname = request.getParameter("fullname");
            String role = "USER";
            Account account = new Account(AppUtils.generateUUID().toString());
            account.setUsername(username);
            account.setPassword(password);
            account.setFullname(fullname);
            account.setRole(role);

            AccountDAO dao = AccountDAO.getInstance();
            boolean isExisted = dao.isExisted(username);
            if (!isExisted) {
                boolean inserted = dao.createUser(account);
                if (inserted) {
//                    AccountClient client = new AccountClient();
//                    Account loginAccount = client.checkLogin(Account.class, username, password);
//                    request.setAttribute("ACCOUNT", loginAccount);
                    url = LOGIN + "?username=" + username + "&password=" + password;
                }
            } else {
                request.setAttribute("DUPLICATE", username);
                url = REJECT;
            }
        } catch (Exception e) {
            url = REJECT;
        } finally {
            request.getRequestDispatcher(url).forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
