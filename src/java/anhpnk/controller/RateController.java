/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.controller;

import anhpnk.daos.FoodDAO;
import anhpnk.daos.RecipeDAO;
import anhpnk.entity.Account;
import anhpnk.entity.FavoriteRecipe;
import anhpnk.entity.Food;
import anhpnk.entity.Recipe;
import anhpnk.utils.AppUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Kim Anh
 */
public class RateController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String recipeId = request.getParameter("recipe");
            
            RecipeDAO dao = RecipeDAO.getInstance();
            Recipe recipe = dao.getOne(recipeId);
            if (recipe != null) {
                HttpSession session = request.getSession();
                Account account = (Account) session.getAttribute("ACCOUNT");
                FavoriteRecipe fr = new FavoriteRecipe(AppUtils.generateUUID().toString());
                fr.setAccountId(account);
                fr.setRecipeId(recipe);
                boolean inserted = dao.insertFavoriteRecipe(fr);
                request.setAttribute("INSERT", inserted);
            }
            request.setAttribute("RECIPE", recipe);
        } catch (Exception e) {
            Logger.getLogger(RateController.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            request.getRequestDispatcher("recipe_backup.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
