/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.controller;

import anhpnk.constant.DictionaryConstant;
import anhpnk.daos.FoodDAO;
import anhpnk.daos.IngredientDAO;
//import anhpnk.daos.RecipeDAO;
import anhpnk.entity.Food;
import anhpnk.entity.FoodRecipe;
import anhpnk.entity.Ingredient;
import anhpnk.entity.Recipe;
import anhpnk.utils.AppUtils;
import anhpnk.utils.SuggestionUtils;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Kim Anh
 */
public class SuggestController extends HttpServlet {

    private final String DAILY_MEAL = "DAILY_MEAL";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String numOfPeopleStr = request.getParameter("numOfPeople");
            int numOfPeople = Integer.parseInt(numOfPeopleStr);
            request.setAttribute("PEOPLE", numOfPeople);
            List<Recipe> suggestRecipeList = new ArrayList<>();

            IngredientDAO iDao = IngredientDAO.getInstance();
            List<Ingredient> ingredients = iDao.getAll();
            FoodDAO dao = FoodDAO.getInstance();
            for (int i = 0; i < 6; i++) {

                Recipe recipe = new Recipe(AppUtils.generateUUID().toString());
                recipe.setCategory(DAILY_MEAL);
                recipe.setPoint(0);

                List<Food> suggestionList = null;
                double recipePrice = 0;
                String recipeName = "-";
                List<FoodRecipe> recipeFoodList = new ArrayList<>();

                Random r = new Random();
                int categoryIndex = r.nextInt(DictionaryConstant.RECIPE_CATEGORY.length);
                String recipeCategoryString = DictionaryConstant.RECIPE_CATEGORY[categoryIndex];
                recipe.setCategory(recipeCategoryString);
                String[] categories = recipeCategoryString.split("-");
                for (String category : categories) {
                    suggestionList = dao.getFoodByCategory(category);
                    boolean flag = false;
                    boolean isValidFood = false;
                    Food selectedFood = null;
                    double selectedPrice = 0;
                    while (!flag) {
                        int foodIndex = r.nextInt(suggestionList.size());
                        selectedFood = suggestionList.get(foodIndex);
                        isValidFood = SuggestionUtils.isValidFoodName(selectedFood.getName(), category);
                        selectedPrice = SuggestionUtils.getSuggestionPrice(selectedFood, ingredients);
                        if (isValidFood) {
                            flag = true;
                            FoodRecipe fr = new FoodRecipe(AppUtils.generateUUID().toString());
                            fr.setFoodId(selectedFood);
                            recipeFoodList.add(fr);
                            recipePrice += selectedPrice;
                            recipeName += selectedFood.getCategoryId().getName() + "-";
                        }
                    }
                }
                recipe.setPrice(recipePrice * numOfPeople);
                recipe.setFoodRecipeCollection(recipeFoodList);
                recipe.setName(recipeName);
                suggestRecipeList.add(recipe);
            }
//            }
            request.setAttribute("RECIPE_LIST", suggestRecipeList);

        } catch (Exception e) {
            Logger.getLogger(SuggestController.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            request.getRequestDispatcher("suggestion.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
