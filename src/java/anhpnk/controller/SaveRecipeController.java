/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.controller;

import anhpnk.daos.FoodDAO;
import anhpnk.daos.RecipeDAO;
import anhpnk.entity.Account;
import anhpnk.entity.FavoriteRecipe;
import anhpnk.entity.Food;
import anhpnk.entity.FoodRecipe;
import anhpnk.entity.Recipe;
import anhpnk.utils.AppUtils;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author Kim Anh
 */
public class SaveRecipeController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            String recipeId = request.getParameter("id");
            String recipeName = request.getParameter("name");
            String recipeCategory = request.getParameter("category");
            String recipePrice = request.getParameter("price");
            Recipe recipe = new Recipe(recipeId, new String(recipeName.getBytes(StandardCharsets.ISO_8859_1), StandardCharsets.UTF_8), recipeCategory, 1, Double.parseDouble(recipePrice));
            
            List<FoodRecipe> frs = new ArrayList<>();
            FoodDAO fDao = FoodDAO.getInstance();
            Food f1 = fDao.getById(request.getParameter("f1"));
            Food f2 = fDao.getById(request.getParameter("f2"));
            Food f3 = fDao.getById(request.getParameter("f3"));
            frs.add(new FoodRecipe(AppUtils.generateUUID().toString(), f1, recipe));
            frs.add(new FoodRecipe(AppUtils.generateUUID().toString(), f2, recipe));
            frs.add(new FoodRecipe(AppUtils.generateUUID().toString(), f3, recipe));
            recipe.setFoodRecipeCollection(frs);
            
            RecipeDAO rDao = RecipeDAO.getInstance();
            boolean inserted = rDao.insertFullRecipe(recipe);
            
            if (inserted) {
                HttpSession session = request.getSession();
                Account account = (Account) session.getAttribute("ACCOUNT");
                FavoriteRecipe fr = new FavoriteRecipe(AppUtils.generateUUID().toString());
                fr.setAccountId(account);
                fr.setRecipeId(recipe);
                boolean insertFavorite = rDao.insertFavoriteRecipe(fr);
            }
            request.setAttribute("RECIPE", recipe);
            request.setAttribute("F1", f1);
            request.setAttribute("F2", f2);
            request.setAttribute("F3", f3);
            request.setAttribute("INSERT", inserted);
        } catch (Exception e) {
        } finally {
            request.getRequestDispatcher("recipe_suggestion.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
