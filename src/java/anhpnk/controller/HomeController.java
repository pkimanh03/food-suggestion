/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.controller;

import anhpnk.daos.CategoryDAO;
import anhpnk.daos.FoodDAO;
import anhpnk.daos.IngredientDAO;
import anhpnk.dtos.FoodDTO;
import anhpnk.entity.Food;
import anhpnk.entity.FoodCategory;
import anhpnk.entity.FoodIngredient;
import anhpnk.entity.Ingredient;
import anhpnk.utils.SuggestionUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Kim Anh
 */
public class HomeController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try {
            CategoryDAO cDao = CategoryDAO.getInstance();
            List<FoodCategory> categories = cDao.getAll();
            request.setAttribute("CATEGORIES", categories);
            
            FoodDAO fDao = FoodDAO.getInstance();
            String pageStr = request.getParameter("page");
            System.out.println("pagestr: " + pageStr);
            int page = 1;
            if (pageStr != null) {
                page = Integer.parseInt(pageStr);
            }
            request.setAttribute("CURRENT_PAGE", page);
            List<Food> foods = null;
            String categoryId = request.getParameter("category");
            if (categoryId == null || categoryId.isEmpty()) {
                request.setAttribute("CATEGORY", "");
                int numOfPage = fDao.getNumberOfPage();
                System.out.println("num:" + numOfPage);
                if (page > numOfPage) {
                    request.setAttribute("CURRENT_PAGE", 1);
                    foods = fDao.getFoodWithPaging(1);
                } else if (page < 1) {
                    request.setAttribute("CURRENT_PAGE", numOfPage);
                    foods = fDao.getFoodWithPaging(numOfPage);
                } else {
                    foods = fDao.getFoodWithPaging(page);
                }
            } else {
                request.setAttribute("CATEGORY_PARAM", categoryId);
                long numOfPageC = fDao.getNumberOfPageForEachCategory(categoryId);
                if (page > numOfPageC) {
                    foods = fDao.getFoodWithPagingForEachCategory(1, categoryId);
                } else if (page < 1) {
                    foods = fDao.getFoodWithPagingForEachCategory(numOfPageC, categoryId);
                } else {
                    foods = fDao.getFoodWithPagingForEachCategory(page, categoryId);
                }
            }
            
            IngredientDAO iDao = IngredientDAO.getInstance();
            List<Ingredient> ingredients = iDao.getAll();
            List<FoodDTO> dtos = new ArrayList<>();
            for (Food f : foods) {
                double price = SuggestionUtils.getSuggestionPrice(f, ingredients);
                FoodDTO dto = new FoodDTO();
                dto.setFoodIngredientCollection(f.getFoodIngredientCollection());
                dto.setId(f.getId());
                dto.setImgLink(f.getImg());
                dto.setLink(f.getLink());
                dto.setName(f.getName());
                dto.setPrice(price);
                dto.setQuanity(f.getQuantity());
                
                dtos.add(dto);
            }
            request.setAttribute("FOODS", dtos);
        } catch (Exception e) {
            e.printStackTrace();
            Logger.getLogger(HomeController.class.getName()).log(Level.SEVERE, null, e);
        } finally {
            request.getRequestDispatcher("homepage.jsp").forward(request, response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
