/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.entity;

import anhpnk.jaxb.foody.IngredientFood;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Kim Anh
 */
@Entity
@Table(name = "FOOD_INGREDIENT")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FoodIngredient.findAll", query = "SELECT f FROM FoodIngredient f")
    , @NamedQuery(name = "FoodIngredient.findById", query = "SELECT f FROM FoodIngredient f WHERE f.id = :id")
    , @NamedQuery(name = "FoodIngredient.findByName", query = "SELECT f FROM FoodIngredient f WHERE f.name = :name")
    , @NamedQuery(name = "FoodIngredient.findByQuantity", query = "SELECT f FROM FoodIngredient f WHERE f.quantity = :quantity")})
public class FoodIngredient implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private String id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "quantity")
    private String quantity;
    @JoinColumn(name = "food_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private Food foodId;

    public FoodIngredient() {
    }

    public FoodIngredient(String id) {
        this.id = id;
    }

    public FoodIngredient(String id, String name, String quantity) {
        this.id = id;
        this.name = name;
        this.quantity = quantity;
    }

    public FoodIngredient(IngredientFood foodIngzredient) {
        this.id = foodIngzredient.getId();
        this.name = foodIngzredient.getName();
        this.quantity = foodIngzredient.getQuantity();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public Food getFoodId() {
        return foodId;
    }

    public void setFoodId(Food foodId) {
        this.foodId = foodId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FoodIngredient)) {
            return false;
        }
        FoodIngredient other = (FoodIngredient) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FoodIngredient{" + "id=" + id + ", name=" + name + ", quantity=" + quantity + ", foodId=" + foodId + '}';
    }

    
}
