/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.entity;

import anhpnk.jaxb.foody.Category;
import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kim Anh
 */
@Entity
@Table(name = "FOOD_CATEGORY")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "FoodCategory.findAll", query = "SELECT f FROM FoodCategory f")
    , @NamedQuery(name = "FoodCategory.findById", query = "SELECT f FROM FoodCategory f WHERE f.id = :id")
    , @NamedQuery(name = "FoodCategory.findByName", query = "SELECT f FROM FoodCategory f WHERE f.name = :name")
    , @NamedQuery(name = "FoodCategory.findByLink", query = "SELECT f FROM FoodCategory f WHERE f.link = :link")})
public class FoodCategory implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private String id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "link")
    private String link;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "categoryId")
    private Collection<Food> foodCollection;

    public FoodCategory() {
    }

    public FoodCategory(String id) {
        this.id = id;
    }

    public FoodCategory(String id, String name, String link) {
        this.id = id;
        this.name = name;
        this.link = link;
    }

    public FoodCategory(Category category) {
        this.id = category.getId();
        this.name = category.getName();
        this.link = category.getLink();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    @XmlTransient
    public Collection<Food> getFoodCollection() {
        return foodCollection;
    }

    public void setFoodCollection(Collection<Food> foodCollection) {
        this.foodCollection = foodCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof FoodCategory)) {
            return false;
        }
        FoodCategory other = (FoodCategory) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "FoodCategory{" + "id=" + id + ", name=" + name + ", link=" + link + '}';
    }
    
}
