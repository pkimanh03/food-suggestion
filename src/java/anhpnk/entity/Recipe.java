/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kim Anh
 */
@Entity
@Table(name = "RECIPE")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Recipe.findAll", query = "SELECT r FROM Recipe r")
    , @NamedQuery(name = "Recipe.findById", query = "SELECT r FROM Recipe r WHERE r.id = :id")
    , @NamedQuery(name = "Recipe.findByName", query = "SELECT r FROM Recipe r WHERE r.name = :name")
    , @NamedQuery(name = "Recipe.findByCategory", query = "SELECT r FROM Recipe r WHERE r.category = :category")
    , @NamedQuery(name = "Recipe.findByPoint", query = "SELECT r FROM Recipe r WHERE r.point = :point")
    , @NamedQuery(name = "Recipe.findByPrice", query = "SELECT r FROM Recipe r WHERE r.price = :price")})
public class Recipe implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "id")
    private String id;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1073741823)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1073741823)
    @Column(name = "category")
    private String category;
    @Basic(optional = false)
    @NotNull
    @Column(name = "point")
    private int point;
    @Basic(optional = false)
    @NotNull
    @Column(name = "price")
    private double price;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "recipeId")
    private Collection<FoodRecipe> foodRecipeCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "recipeId")
    private Collection<FavoriteRecipe> favoriteRecipeCollection;

    public Recipe() {
    }

    public Recipe(String id) {
        this.id = id;
    }

    public Recipe(String id, String name, String category, int point, double price) {
        this.id = id;
        this.name = name;
        this.category = category;
        this.point = point;
        this.price = price;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @XmlTransient
    public Collection<FoodRecipe> getFoodRecipeCollection() {
        return foodRecipeCollection;
    }

    public void setFoodRecipeCollection(Collection<FoodRecipe> foodRecipeCollection) {
        this.foodRecipeCollection = foodRecipeCollection;
    }

    @XmlTransient
    public Collection<FavoriteRecipe> getFavoriteRecipeCollection() {
        return favoriteRecipeCollection;
    }

    public void setFavoriteRecipeCollection(Collection<FavoriteRecipe> favoriteRecipeCollection) {
        this.favoriteRecipeCollection = favoriteRecipeCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Recipe)) {
            return false;
        }
        Recipe other = (Recipe) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "anhpnk.entity.Recipe[ id=" + id + " ]";
    }
    
}
