/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.entity;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Kim Anh
 */
@Entity
@Table(name = "FOOD")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Food.findAll", query = "SELECT f FROM Food f")
    , @NamedQuery(name = "Food.findById", query = "SELECT f FROM Food f WHERE f.id = :id")
    , @NamedQuery(name = "Food.findByName", query = "SELECT f FROM Food f WHERE f.name = :name")
    , @NamedQuery(name = "Food.findByImg", query = "SELECT f FROM Food f WHERE f.img = :img")
    , @NamedQuery(name = "Food.findByLink", query = "SELECT f FROM Food f WHERE f.link = :link")
    , @NamedQuery(name = "Food.findByQuantity", query = "SELECT f FROM Food f WHERE f.quantity = :quantity")})
public class Food implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id")
    private String id;
    @Basic(optional = false)
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "img")
    private String img;
    @Basic(optional = false)
    @Column(name = "link")
    private String link;
    @Basic(optional = false)
    @Column(name = "quantity")
    private int quantity;
    @JoinTable(name = "FOOD_RECIPE", joinColumns = {
        @JoinColumn(name = "food_id", referencedColumnName = "id")}, inverseJoinColumns = {
        @JoinColumn(name = "recipe_id", referencedColumnName = "id")})
    @ManyToMany(fetch = FetchType.EAGER)
    private Collection<Recipe> recipeCollection;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "foodId")
    private Collection<FoodIngredient> foodIngredientCollection;
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    @ManyToOne(optional = false)
    private FoodCategory categoryId;

    public Food() {
    }

    public Food(String id) {
        this.id = id;
    }

    public Food(String id, String name, String img, String link, int quantity) {
        this.id = id;
        this.name = name;
        this.img = img;
        this.link = link;
        this.quantity = quantity;
    }

    public Food(anhpnk.jaxb.foody.Food food) {
        this.id = food.getId();
        this.name = food.getName();
        this.img = food.getImg();
        this.link = food.getLink();
        this.quantity = Integer.parseInt(food.getQuantity());
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @XmlTransient
    public Collection<Recipe> getRecipeCollection() {
        return recipeCollection;
    }

    public void setRecipeCollection(Collection<Recipe> recipeCollection) {
        this.recipeCollection = recipeCollection;
    }

    @XmlTransient
    public Collection<FoodIngredient> getFoodIngredientCollection() {
        return foodIngredientCollection;
    }

    public void setFoodIngredientCollection(Collection<FoodIngredient> foodIngredientCollection) {
        this.foodIngredientCollection = foodIngredientCollection;
    }

    public FoodCategory getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(FoodCategory categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Food)) {
            return false;
        }
        Food other = (Food) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Food{" + "id=" + id + ", name=" + name + ", img=" + img + ", link=" + link + ", quantity=" + quantity + ", categoryId=" + categoryId + '}';
    }

    
}
