/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.threader;

import anhpnk.constant.URLConstant;
import anhpnk.crawler.FoodCategoriesCrawler;
import anhpnk.crawler.FoodCrawler;
import anhpnk.jaxb.foody.Category;
import anhpnk.jaxb.foody.Food;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;

/**
 *
 * @author Kim Anh
 */
public class FoodThread extends BaseThread implements Runnable {

    public static List<Food> listAllFood = new ArrayList();
    private ServletContext context;

    public FoodThread(ServletContext context) {
        this.context = context;
    }

    @Override
    public void run() {
        try {
            FoodCategoriesCrawler categoriesCrawler = new FoodCategoriesCrawler(context);
            List<Category> categories = categoriesCrawler.getCategories(URLConstant.FOOD_URL);

            for (Category category : categories) {
                Thread crawlingEachPageThread = new Thread(new FoodCrawler(category.getLink(), category, context));
                crawlingEachPageThread.start();
            }
        } catch (Exception e) {
            Logger.getLogger(FoodThread.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
