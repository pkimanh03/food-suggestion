/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.threader;

import anhpnk.constant.AppConstant;
import anhpnk.constant.URLConstant;
import anhpnk.crawler.IngredientLinkCrawler;
import anhpnk.crawler.IngredientsCrawler;
import anhpnk.crawler.SecondIngredientCrawler;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;

/**
 *
 * @author Kim Anh
 */
public class TTTTIngredientsThread extends BaseThread implements Runnable {

    private ServletContext context;

    public TTTTIngredientsThread(ServletContext context) {
        this.context = context;
    }

    @Override
    public void run() {
        try {
            Thread ingredientCrawlerThread2 = new Thread(new SecondIngredientCrawler(context, URLConstant.SECOND_INGREDIENT_URL));
            ingredientCrawlerThread2.start();

        } catch (Exception e) {
            Logger.getLogger(FoodThread.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
