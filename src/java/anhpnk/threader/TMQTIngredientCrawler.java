/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.threader;

import anhpnk.constant.URLConstant;
import anhpnk.crawler.IngredientLinkCrawler;
import anhpnk.crawler.IngredientsCrawler;
import anhpnk.crawler.SecondIngredientCrawler;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletContext;

/**
 *
 * @author Kim Anh
 */
public class TMQTIngredientCrawler extends BaseThread {
    private ServletContext context;

    public TMQTIngredientCrawler(ServletContext context) {
        this.context = context;
    }

    @Override
    public void run() {
        try {
                IngredientLinkCrawler ingredientCategoryCrawler = new IngredientLinkCrawler(context);
                String link = ingredientCategoryCrawler.getLinkIngredient(URLConstant.INGREDIENTS_URL);
                
                Thread ingredientCrawlerThread = new Thread(new IngredientsCrawler(context, link));
                ingredientCrawlerThread.start();

        } catch (Exception e) {
            Logger.getLogger(TMQTIngredientCrawler.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
