/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.dtos;

import java.io.Serializable;

/**
 *
 * @author Kim Anh
 */
public class IngredientDTO implements Serializable {
    private String id;
    private String name;
    private double price;
    private String unit;

    public IngredientDTO(String name, double price, String unit) {
        this.name = name;
        this.price = price;
        this.unit = unit;
    }

    public IngredientDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }
    
    @Override
    public String toString() {
        return name + "-" + unit + "-" + price;
    }


    
}
