/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.dtos;

import java.io.Serializable;

/**
 *
 * @author Kim Anh
 */
public class FoodRecipeDTO implements Serializable {
    private String foodId, recipeId;

    public FoodRecipeDTO(String foodId, String recipeId) {
        this.foodId = foodId;
        this.recipeId = recipeId;
    }

    public String getFoodId() {
        return foodId;
    }

    public void setFoodId(String foodId) {
        this.foodId = foodId;
    }

    public String getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(String recipeId) {
        this.recipeId = recipeId;
    }
    
}
