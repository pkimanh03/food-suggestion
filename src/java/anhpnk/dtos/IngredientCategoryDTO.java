/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.dtos;

/**
 *
 * @author Kim Anh
 */
public class IngredientCategoryDTO {
    private String id;
    private String name;

    public IngredientCategoryDTO(String name) {
        this.name = name;
    }
    
    @Override
    public String toString() {
        return name;
    }
}
