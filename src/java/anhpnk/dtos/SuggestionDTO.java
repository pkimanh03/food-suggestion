/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.dtos;

import anhpnk.entity.Food;
import java.io.Serializable;
import java.util.List;

/**
 *
 * @author Kim Anh
 */
public class SuggestionDTO implements Serializable {
    private FoodDTO i1, i2, i3;
    private double price;
    private List<FoodDTO> list;

    public SuggestionDTO(List<FoodDTO> list) {
        this.i1 = list.get(0);
        this.i2 = list.get(1);
        this.i3 = list.get(2);
        this.list = list;
        this.price = i1.getPrice() + i2.getPrice() + i3.getPrice();
    }

    public FoodDTO getI1() {
        return list.get(0);
    }

    public void setI1(FoodDTO i1) {
        this.i1 = i1;
    }

    public FoodDTO getI2() {
        return list.get(1);
    }

    public void setI2(FoodDTO i2) {
        this.i2 = i2;
    }

    public FoodDTO getI3() {
        return list.get(2);
    }

    public void setI3(FoodDTO i3) {
        this.i3 = i3;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
    
}
