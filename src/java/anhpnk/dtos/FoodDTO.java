/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package anhpnk.dtos;

import anhpnk.entity.FoodIngredient;
import java.util.Collection;

/**
 *
 * @author Kim Anh
 */
public class FoodDTO {
    private String id, name, imgLink, link;
    private int quanity;
    private double price;
    private Collection<FoodIngredient> foodIngredientCollection;

    public FoodDTO(String id, String name, int quanity, double price) {
        this.id = id;
        this.name = name;
        this.quanity = quanity;
        this.price = price;
    }

    public FoodDTO() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getQuanity() {
        return quanity;
    }

    public void setQuanity(int quanity) {
        this.quanity = quanity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getImgLink() {
        return imgLink;
    }

    public void setImgLink(String imgLink) {
        this.imgLink = imgLink;
    }

    public Collection<FoodIngredient> getFoodIngredientCollection() {
        return foodIngredientCollection;
    }

    public void setFoodIngredientCollection(Collection<FoodIngredient> foodIngredientCollection) {
        this.foodIngredientCollection = foodIngredientCollection;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
    
}
